abstract class ODBC {
    public db: any | null;
    public path: string;
    public type: string;

    constructor(path: string, type: string) {
        this.path = path;
        this.type = type;
    }

    abstract open(): boolean;

    // 执行一些变更数据库结构的 SQL 语句，包括：建表、修改表结构等
    // 返回值：this
    abstract exec(sqlStr: string, params: any[]): Promise<boolean>;

    // 执行数据库内容的变更性 SQL 语句，包括：删、改
    abstract run(sqlStr: string, params: any[]): Promise<{ lastID: string, changes: null }>;

    // 执行数据库内容的变更性 SQL 语句，批量新增数据
    abstract insertByParamsArr(sqlStr: string, objs: [][]): Promise<boolean>;

    // 执行常规的查询语句
    abstract all(sqlStr: string, param: []): Promise<any[]>;

    abstract close(): Promise<boolean>;

    abstract backup(dbPath: string): Promise<boolean>;
}

export default ODBC;