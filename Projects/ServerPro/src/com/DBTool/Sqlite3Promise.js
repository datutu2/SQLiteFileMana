var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./ODBC", "sqlite3"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const ODBC_1 = __importDefault(require("./ODBC"));
    const sqlite3_1 = __importDefault(require("sqlite3"));
    const sqlite3 = sqlite3_1.default.verbose();
    class Sqlite3Promise extends ODBC_1.default {
        constructor(path, type = 'file') {
            super(path, type);
        }
        open() {
            if (this.db) {
                return this.db;
            }
            else if (this.type === 'memory') {
                this.db = new sqlite3.Database(':memory:');
                return this.db;
            }
            else if (this.type === 'file') {
                this.db = new sqlite3.Database(this.path);
            }
            else {
                console.log(`创建数据库失败，无法识别的类型${this.type}，仅支持：【file】 or 【memory】`);
                return null;
            }
        }
        ;
        async run(sqlStr, params) {
            let that = this;
            return new Promise(function (resolve, reject) {
                if (!params)
                    params = [];
                that.db.run(sqlStr, params, function (err) {
                    if (err) {
                        console.log(err);
                        reject('执行SQL语句失败：' + err.message);
                    }
                    else {
                        let { lastID, changes } = this;
                        resolve({ lastID, changes });
                    }
                });
            });
        }
        ;
        async all(query, params) {
            let that = this;
            return new Promise(function (resolve, reject) {
                if (!params)
                    params = [];
                that.db.all(query, params, function (err, row) {
                    if (err) {
                        console.log(err);
                    }
                    resolve(row || []);
                });
            });
        }
        async exec(sql) {
            let that = this;
            return new Promise(function (resolve, reject) {
                that.db.exec(sql, function (err, rows) {
                    if (err) {
                        reject('Read error: ' + err.message);
                    }
                    else {
                        resolve(true);
                    }
                });
            });
        }
        ;
        async beginTransaction() {
            await this.exec('BEGIN TRANSACTION;;');
        }
        ;
        async commitTransaction() {
            await this.exec('COMMIT TRANSACTION;');
        }
        ;
        async insertData(sql, tileData) {
            let that = this;
            return new Promise(function (resolve) {
                that.db.run(sql, tileData, function (err) {
                    let { lastID, changes } = this;
                    if (err) {
                        console.log(err);
                    }
                    resolve({ lastID, changes });
                });
            });
        }
        ;
        insertByParamsArr(sqlStr, objs) {
            return Promise.resolve(false);
        }
        async close() {
            if (this.db && this.db.close) {
                return new Promise((resolve) => {
                    this.db.close((err) => {
                        if (err) {
                            resolve(false);
                        }
                        else {
                            resolve(true);
                        }
                    });
                });
            }
            else {
                return false;
            }
        }
        ;
        backup(dbPath) {
            return Promise.resolve(false);
        }
        insertByObjs(sqlStr, objs) {
            return Promise.resolve({ changes: null, lastID: '' });
        }
    }
    exports.default = Sqlite3Promise;
});
