import path from 'path';
import fsPromise from 'fs-promise';
// @ts-ignore
import colors from 'colors-console';
import sleep from '../lib/sleep';
import RSATool from '../lib/RSATool';
import fs from 'fs';

let configInit = false;

const defaultConfig = {
    'version': 'v2.6',
    'clientCORS': true,
    'clientServerPort': 3000,
    'manageServerPort': 3001,
    'wsServerPort': 3001,
    'clientBlackList': [],
    'manageWhiteList': [
        '::ffff:127.0.0.1'
    ],
    'wwwPath': 'www',
    'Cesium': {
        'version': '1.112',
        'IonAccessToken': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIzN2UwMjViMC0xOTJhLTQxMmItOWEzMS1kMjJiNmZiY2E5N2YiLCJpZCI6MjU5LCJpYXQiOjE3MDE0NTc1NjB9.bMNhc7S4cuG1qUnRUiLxr5lunSH1k6-UfkRXSqVFYyM'
    },
    'MapBox': {
        'access_token': 'pk.eyJ1Ijoic3ZjLW9rdGEtbWFwYm94LXN0YWZmLWFjY2VzcyIsImEiOiJjbG5sMnExa3kxNTJtMmtsODJld24yNGJlIn0.RQ4CHchAYPJQZSiUJ0O3VQ',
        'header': {
            'Referer': 'https://geojson.io/'
        }
    },
    'CacheSave': {
        // 缓存到文件
        'CacheSaveFile': true,
        // 缓存到数据库
        'CacheSaveDB': false
    },
    'userList': [
        {'userName': 'user01', 'nickname': '访客', 'password': '123456', 'role': 'client'},
        {'userName': 'user02', 'nickname': '访客', 'password': '123456', 'role': 'client'},
        {'userName': 'user03', 'nickname': '访客', 'password': '123456', 'role': 'client'},
        {'userName': 'admin', 'nickname': '管理员', 'password': '123456', 'role': 'manage'}
    ],
    'publicDer': null,
    'privateDer': null,
};

const configTool = {
    appBasePath: '',
    wwwPath: '',
    config: {} as any,
    async editConfig(key: string, value: any) {
        await this.init();
        let result = false;
        if (typeof this.config[key] === typeof value) {
            this.config[key] = value;
            await fsPromise.writeJSON(path.join(this.appBasePath, '/conf/config.json'), this.config);
            result = true;
        }
        return result;
    },
    async init() {
        return new Promise(async (resolve, reject) => {
            if (!configInit) {
                configInit = true;

                // 是否处于开发环境
                let isDev = !__dirname.includes('snapshot');

                if (isDev) {
                    this.appBasePath = path.join(__dirname, '../../');
                } else {
                    this.appBasePath = path.dirname(process.execPath);
                }

                await fsPromise.ensureDir(path.join(this.appBasePath, '/conf'));
                await fsPromise.ensureDir(path.join(this.appBasePath, '/temp/fileOut'));
                await fsPromise.ensureDir(path.join(this.appBasePath, '/temp/fileDB'));
                await fsPromise.ensureDir(path.join(this.appBasePath, '/temp/logs/clientServer'));
                await fsPromise.ensureDir(path.join(this.appBasePath, '/temp/logs/manageServerPort'));
                await fsPromise.ensureDir(path.join(this.appBasePath, '/www'));

                let configFile = path.join(this.appBasePath, '/conf/config.json');

                // 判断是否读取本地已有的配置文件
                await fsPromise.readJson(configFile).then((d: any) => {
                    if (d.version === defaultConfig.version) {
                        this.config = d;
                    } else {
                        console.log(colors('red', '----|||配置文件版本过低，重置配置文件|||----'));
                        throw new Error('配置文件版本不匹配');
                    }
                }).catch(() => {
                    // 新生成一份配置文件
                    this.config = defaultConfig;
                    const {privateDer, publicDer} = RSATool.createKey(2048);
                    this.config.privateDer = privateDer;
                    this.config.publicDer = publicDer;

                    fsPromise.writeJSON(configFile, this.config);
                });

                // 判断wwwPath是否是绝对路径
                if (path.isAbsolute(this.config.wwwPath)) {
                    this.wwwPath = path.join(this.config.wwwPath);
                } else {
                    this.wwwPath = path.join(this.appBasePath, this.config.wwwPath);
                }
            }

            while (!configInit) {
                await sleep(50);
            }
            resolve(true);
        });

    },
    preservableONDB() {
        return this.config.CacheSave.CacheSaveDB;
    },
    preservableONFile() {
        return this.config.CacheSave.CacheSaveFile;
    }
};


export default configTool;
