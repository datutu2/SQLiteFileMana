var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "path", "../configTool", "koa-log4"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const path_1 = __importDefault(require("path"));
    const configTool_1 = __importDefault(require("../configTool"));
    const koa_log4_1 = __importDefault(require("koa-log4"));
    const log4jsConf = {
        appenders: {
            out: {
                type: 'console'
            },
            access: {
                type: 'dateFile',
                pattern: '-yyyy-MM-dd.log',
                alwaysIncludePattern: true,
                encoding: 'utf-8',
                filename: path_1.default.join(configTool_1.default.appBasePath, './logs/http/access')
            },
            application: {
                type: 'dateFile',
                pattern: '-yyyy-MM-dd.log',
                alwaysIncludePattern: true,
                encoding: 'utf-8',
                filename: path_1.default.join(configTool_1.default.appBasePath, './logs/http/application')
            }
        },
        categories: {
            default: { appenders: ['out'], level: 'info' },
            access: { appenders: ['access'], level: 'info' },
            application: { appenders: ['application'], level: 'WARN' }
        }
    };
    koa_log4_1.default.configure(log4jsConf);
    exports.accessLogger = () => koa_log4_1.default.koaLogger(koa_log4_1.default.getLogger('access'));
    exports.systemLogger = koa_log4_1.default.getLogger('application');
});
