var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "path", "fs-promise", "colors-console", "../lib/sleep", "../lib/RSATool"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const path_1 = __importDefault(require("path"));
    const fs_promise_1 = __importDefault(require("fs-promise"));
    const colors_console_1 = __importDefault(require("colors-console"));
    const sleep_1 = __importDefault(require("../lib/sleep"));
    const RSATool_1 = __importDefault(require("../lib/RSATool"));
    let configInit = false;
    const defaultConfig = {
        'version': 'v2.6',
        'clientCORS': true,
        'clientServerPort': 3000,
        'manageServerPort': 3001,
        'wsServerPort': 3001,
        'clientBlackList': [],
        'manageWhiteList': [
            '::ffff:127.0.0.1'
        ],
        'wwwPath': 'www',
        'Cesium': {
            'version': '1.112',
            'IonAccessToken': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIzN2UwMjViMC0xOTJhLTQxMmItOWEzMS1kMjJiNmZiY2E5N2YiLCJpZCI6MjU5LCJpYXQiOjE3MDE0NTc1NjB9.bMNhc7S4cuG1qUnRUiLxr5lunSH1k6-UfkRXSqVFYyM'
        },
        'MapBox': {
            'access_token': 'pk.eyJ1Ijoic3ZjLW9rdGEtbWFwYm94LXN0YWZmLWFjY2VzcyIsImEiOiJjbG5sMnExa3kxNTJtMmtsODJld24yNGJlIn0.RQ4CHchAYPJQZSiUJ0O3VQ',
            'header': {
                'Referer': 'https://geojson.io/'
            }
        },
        'CacheSave': {
            'CacheSaveFile': true,
            'CacheSaveDB': false
        },
        'userList': [
            { 'userName': 'user01', 'nickname': '访客', 'password': '123456', 'role': 'client' },
            { 'userName': 'user02', 'nickname': '访客', 'password': '123456', 'role': 'client' },
            { 'userName': 'user03', 'nickname': '访客', 'password': '123456', 'role': 'client' },
            { 'userName': 'admin', 'nickname': '管理员', 'password': '123456', 'role': 'manage' }
        ],
        'publicDer': null,
        'privateDer': null,
    };
    const configTool = {
        appBasePath: '',
        wwwPath: '',
        config: {},
        async editConfig(key, value) {
            await this.init();
            let result = false;
            if (typeof this.config[key] === typeof value) {
                this.config[key] = value;
                await fs_promise_1.default.writeJSON(path_1.default.join(this.appBasePath, '/conf/config.json'), this.config);
                result = true;
            }
            return result;
        },
        async init() {
            return new Promise(async (resolve, reject) => {
                if (!configInit) {
                    configInit = true;
                    let isDev = !__dirname.includes('snapshot');
                    if (isDev) {
                        this.appBasePath = path_1.default.join(__dirname, '../../');
                    }
                    else {
                        this.appBasePath = path_1.default.dirname(process.execPath);
                    }
                    await fs_promise_1.default.ensureDir(path_1.default.join(this.appBasePath, '/conf'));
                    await fs_promise_1.default.ensureDir(path_1.default.join(this.appBasePath, '/temp/fileOut'));
                    await fs_promise_1.default.ensureDir(path_1.default.join(this.appBasePath, '/temp/fileDB'));
                    await fs_promise_1.default.ensureDir(path_1.default.join(this.appBasePath, '/temp/logs/clientServer'));
                    await fs_promise_1.default.ensureDir(path_1.default.join(this.appBasePath, '/temp/logs/manageServerPort'));
                    await fs_promise_1.default.ensureDir(path_1.default.join(this.appBasePath, '/www'));
                    let configFile = path_1.default.join(this.appBasePath, '/conf/config.json');
                    await fs_promise_1.default.readJson(configFile).then((d) => {
                        if (d.version === defaultConfig.version) {
                            this.config = d;
                        }
                        else {
                            console.log((0, colors_console_1.default)('red', '----|||配置文件版本过低，重置配置文件|||----'));
                            throw new Error('配置文件版本不匹配');
                        }
                    }).catch(() => {
                        this.config = defaultConfig;
                        const { privateDer, publicDer } = RSATool_1.default.createKey(2048);
                        this.config.privateDer = privateDer;
                        this.config.publicDer = publicDer;
                        fs_promise_1.default.writeJSON(configFile, this.config);
                    });
                    if (path_1.default.isAbsolute(this.config.wwwPath)) {
                        this.wwwPath = path_1.default.join(this.config.wwwPath);
                    }
                    else {
                        this.wwwPath = path_1.default.join(this.appBasePath, this.config.wwwPath);
                    }
                }
                while (!configInit) {
                    await (0, sleep_1.default)(50);
                }
                resolve(true);
            });
        },
        preservableONDB() {
            return this.config.CacheSave.CacheSaveDB;
        },
        preservableONFile() {
            return this.config.CacheSave.CacheSaveFile;
        }
    };
    exports.default = configTool;
});
