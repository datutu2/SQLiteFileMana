import TileUtil3857 from './TileUtil3857.js';

// 获取地图瓦片几何
function GetMapTile(leftTopLon: number, leftTopLat: number, rightBottomLon: number, rightBottomLat: number, minZoom = 0, maxZoom = 22) {
    let tileUtil = new TileUtil3857();
    const zooms = [];
    if (minZoom === 0) {
        zooms.push(0);
    }
    for (let i = minZoom; i <= maxZoom; i++) {
        let l = tileUtil.getTileXYZ(leftTopLon, leftTopLat, i);
        let r = tileUtil.getTileXYZ(rightBottomLon, rightBottomLat, i);
        if (l.x - r.x !== 0 && l.y - r.y !== 0) {
            zooms.push(i);
        }
    }
    // 根据有瓦片数据的地方下载瓦片
    let imgListAll = [];
    while (zooms.length) {
        let zoom = zooms.shift();
        if (zoom){
            let leftTop = tileUtil.getTileXYZ(leftTopLon, leftTopLat, zoom);
            let rightBottom = tileUtil.getTileXYZ(rightBottomLon, rightBottomLat, zoom);
            for (let x = leftTop.x; x <= rightBottom.x; x++) {
                for (let y = leftTop.y; y <= rightBottom.y; y++) {
                    imgListAll.push({x, y, zoom});
                }
            }
        }
    }
    return imgListAll;
}


export default GetMapTile;
