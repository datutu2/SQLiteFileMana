var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "@turf/turf", "./twoPolygonRelation", "./TileSet"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const turf = __importStar(require("@turf/turf"));
    const twoPolygonRelation_1 = __importDefault(require("./twoPolygonRelation"));
    const TileSet_1 = __importDefault(require("./TileSet"));
    class TileUtil {
        getTilesByGeoJson(geoJson, zoom = 8, log = false) {
            geoJson = geoJson || turf.polygon([[[-180, -90], [180, -90], [180, 90], [-180, 90], [-180, -90]]]);
            geoJson = turf.flatten(geoJson);
            let tileSet = new TileSet_1.default();
            turf.featureEach(geoJson, (currentGeometry) => {
                let tileMap = new Map();
                this.getRootTiles().forEach((rootTile) => {
                    rootTile.type = (0, twoPolygonRelation_1.default)(this.tileXYZToRectanglePolygon(rootTile.x, rootTile.y, rootTile.zoom), currentGeometry);
                    if (rootTile.type === 'disjoint') {
                    }
                    else if (tileMap.has(rootTile.zoom)) {
                        tileMap.get(rootTile.zoom).push(rootTile);
                    }
                    else {
                        tileMap.set(rootTile.zoom, [rootTile]);
                    }
                });
                for (let i = 0; i < zoom; i++) {
                    let parentZoomTileList = tileMap.get(i);
                    let parentZoomTileCount = parentZoomTileList.length;
                    let currentZoomTileList = [];
                    for (let j = 0; j < parentZoomTileList.length; j++) {
                        let parentTile = parentZoomTileList[j];
                        let branchTileList = this.getBranchTile(parentTile.x, parentTile.y, parentTile.zoom);
                        for (let k = 0; k < branchTileList.length; k++) {
                            let branchTile = branchTileList[k];
                            if (parentTile.type === 'partIn') {
                                branchTile.type = 'partIn';
                                currentZoomTileList.push(branchTile);
                            }
                            else if (parentTile.type === 'within' || parentTile.type === 'crosses') {
                                branchTile.type = (0, twoPolygonRelation_1.default)(this.tileXYZToRectanglePolygon(branchTile.x, branchTile.y, branchTile.zoom), currentGeometry);
                                if (branchTile.type !== 'disjoint') {
                                    currentZoomTileList.push(branchTile);
                                }
                            }
                        }
                        if (Math.floor(parentZoomTileCount / 20_0000) && j % Math.ceil(parentZoomTileCount / 100) === 0) {
                            log && console.log('zoom：', i + 1, '，统计进度：', Math.ceil(j / parentZoomTileCount * 100) + '%');
                        }
                    }
                    tileMap.set(i + 1, currentZoomTileList);
                }
                [...tileMap.values()].forEach(function (tileList) {
                    while (tileList.length) {
                        let tile = tileList.pop();
                        tile.zoom === zoom && tileSet.add(tile.x, tile.y, tile.zoom);
                    }
                });
            });
            return tileSet.getAll();
        }
        tileXYZToRectanglePolygon(x, y, zoom) {
            const bb = this.tileXYZToRectangle(x, y, zoom);
            turf.bboxPolygon([bb.xmin, bb.ymin, bb.xmax, bb.ymax]);
            return turf.bboxPolygon([bb.xmin, bb.ymin, bb.xmax, bb.ymax]);
        }
    }
    exports.default = TileUtil;
});
