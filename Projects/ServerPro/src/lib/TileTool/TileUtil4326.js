var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./TileImpl/Tile", "../../lib/TileTool/TileImpl/TileBox", "./TileUtil"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const Tile_1 = __importDefault(require("./TileImpl/Tile"));
    const TileBox_1 = __importDefault(require("../../lib/TileTool/TileImpl/TileBox"));
    const TileUtil_1 = __importDefault(require("./TileUtil"));
    class TileUtil4326 extends TileUtil_1.default {
        constructor() {
            super();
        }
        getTileXYZ(lon, lat, zoom) {
            let xTile = Math.floor((lon + 180) / 360 * (1 << (zoom + 1)));
            let yTile = Math.floor(((lat + 90) / 360) * (1 << (zoom + 1)));
            return new Tile_1.default(xTile, yTile, zoom);
        }
        getRootTiles() {
            return [{ x: 0, y: 0, zoom: 0 }, { x: 1, y: 0, zoom: 0 }];
        }
        getBranchTile(xTile, yTile, zoom) {
            let list = [];
            list.push({ x: xTile * 2, y: yTile * 2, zoom: zoom + 1 });
            list.push({ x: xTile * 2 + 1, y: yTile * 2, zoom: zoom + 1 });
            list.push({ x: xTile * 2, y: yTile * 2 + 1, zoom: zoom + 1 });
            list.push({ x: xTile * 2 + 1, y: yTile * 2 + 1, zoom: zoom + 1 });
            return list;
        }
        tileXYZToRectangle(x, y, zoom) {
            let tileBox = new TileBox_1.default();
            tileBox.xmin = this.tile2lon(x, zoom);
            tileBox.xmax = this.tile2lon(x + 1, zoom);
            tileBox.ymin = this.tile2lat(y + 1, zoom);
            tileBox.ymax = this.tile2lat(y, zoom);
            return tileBox;
        }
        tile2lon(x, z) {
            return x / Math.pow(2.0, (z + 1)) * 360.0 - 180;
        }
        tile2lat(y, z) {
            return y / Math.pow(2.0, (z + 1)) * 360.0 - 90;
        }
    }
    exports.default = TileUtil4326;
});
