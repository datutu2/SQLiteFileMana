var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "path", "./TileSet", "../FSTool/index"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const path_1 = __importDefault(require("path"));
    const TileSet_1 = __importDefault(require("./TileSet"));
    const index_1 = __importDefault(require("../FSTool/index"));
    class CheckLackTile {
        tilePath;
        hasTileList;
        constructor(tilePath) {
            if (tilePath.endsWith('/') || tilePath.endsWith('\\')) {
                this.tilePath = path_1.default.join(tilePath, '');
            }
            else {
                this.tilePath = path_1.default.join(tilePath + '\\', '');
            }
            this.hasTileList = [];
        }
        async scanTileList(scanZoom = -1) {
            let scanPath = scanZoom === -1 ? this.tilePath : this.tilePath + scanZoom + '\\';
            const filePathList = await index_1.default.getFilePathList(scanPath);
            let tileSet = new TileSet_1.default();
            while (filePathList.length) {
                let fileItem = filePathList.pop();
                if (fileItem) {
                    let fileStr = fileItem.replace(path_1.default.join(this.tilePath, ''), '');
                    fileStr = fileStr.replace(path_1.default.extname(fileStr), '');
                    let [zoom, x, y] = fileStr.split('\\');
                    if (!isNaN(Number(zoom)) && !isNaN(Number(x)) && !isNaN(Number(y))) {
                        if (scanZoom === -1 || Number(zoom) === scanZoom) {
                            tileSet.add(Number(x), Number(y), Number(zoom));
                        }
                    }
                }
            }
            this.hasTileList = tileSet.getAll();
            return this.hasTileList;
        }
        async findLackTile(allTileList, scanZoom = -1) {
            let hasTileList = await this.scanTileList(scanZoom);
            let lackTileSet = new TileSet_1.default();
            for (let i = 0; i < allTileList.length; i++) {
                let tile = allTileList[i];
                if (tile.zoom === scanZoom || scanZoom === -1) {
                    lackTileSet.add(tile.x, tile.y, tile.zoom);
                }
            }
            for (let i = 0; i < hasTileList.length; i++) {
                let tile = hasTileList[i];
                if (tile.zoom === scanZoom || scanZoom === -1) {
                    lackTileSet.delete(tile.x, tile.y, tile.zoom);
                }
            }
            return lackTileSet.getAll();
        }
    }
    exports.default = CheckLackTile;
});
