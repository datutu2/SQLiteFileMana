// @ts-ignore
import * as turf from '@turf/turf';

function unionGeoJson(geoJson: any) {
    let sourceGeoJson = turf.flatten(geoJson);

    let resGeoJson: any;
    turf.featureEach(sourceGeoJson, function (currentFeature: any, featureIndex: number) {
        if (featureIndex === 0) {
            resGeoJson = currentFeature;
        } else {
            resGeoJson = turf.union(resGeoJson, currentFeature);
        }
    });

    return turf.flatten(resGeoJson);
}

export default unionGeoJson;
