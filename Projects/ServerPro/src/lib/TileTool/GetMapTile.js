var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./TileUtil3857.js"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const TileUtil3857_js_1 = __importDefault(require("./TileUtil3857.js"));
    function GetMapTile(leftTopLon, leftTopLat, rightBottomLon, rightBottomLat, minZoom = 0, maxZoom = 22) {
        let tileUtil = new TileUtil3857_js_1.default();
        const zooms = [];
        if (minZoom === 0) {
            zooms.push(0);
        }
        for (let i = minZoom; i <= maxZoom; i++) {
            let l = tileUtil.getTileXYZ(leftTopLon, leftTopLat, i);
            let r = tileUtil.getTileXYZ(rightBottomLon, rightBottomLat, i);
            if (l.x - r.x !== 0 && l.y - r.y !== 0) {
                zooms.push(i);
            }
        }
        let imgListAll = [];
        while (zooms.length) {
            let zoom = zooms.shift();
            if (zoom) {
                let leftTop = tileUtil.getTileXYZ(leftTopLon, leftTopLat, zoom);
                let rightBottom = tileUtil.getTileXYZ(rightBottomLon, rightBottomLat, zoom);
                for (let x = leftTop.x; x <= rightBottom.x; x++) {
                    for (let y = leftTop.y; y <= rightBottom.y; y++) {
                        imgListAll.push({ x, y, zoom });
                    }
                }
            }
        }
        return imgListAll;
    }
    exports.default = GetMapTile;
});
