import Tile from './TileImpl/Tile';

import TileBox from '../../lib/TileTool/TileImpl/TileBox';

import TileUtil from './TileUtil';


class TileUtil4326 extends TileUtil {
    constructor() {
        super();
    }

    getTileXYZ(lon: number, lat: number, zoom: number) {
        let xTile = Math.floor((lon + 180) / 360 * (1 << (zoom + 1)));
        let yTile = Math.floor(((lat + 90) / 360) * (1 << (zoom + 1)));
        return new Tile(xTile, yTile, zoom);
    }

    getRootTiles() {
        return [{x: 0, y: 0, zoom: 0}, {x: 1, y: 0, zoom: 0}];
    }

    getBranchTile(xTile: number, yTile: number, zoom: number) {
        let list = [];

        list.push({x: xTile * 2, y: yTile * 2, zoom: zoom + 1});
        list.push({x: xTile * 2 + 1, y: yTile * 2, zoom: zoom + 1});
        list.push({x: xTile * 2, y: yTile * 2 + 1, zoom: zoom + 1});
        list.push({x: xTile * 2 + 1, y: yTile * 2 + 1, zoom: zoom + 1});

        return list;
    }

    tileXYZToRectangle(x: number, y: number, zoom: number) {
        let tileBox = new TileBox();
        tileBox.xmin = this.tile2lon(x, zoom);
        tileBox.xmax = this.tile2lon(x + 1, zoom);
        tileBox.ymin = this.tile2lat(y + 1, zoom);
        tileBox.ymax = this.tile2lat(y, zoom);

        return tileBox;
    }

    tile2lon(x: number, z: number) {
        return x / Math.pow(2.0, (z + 1)) * 360.0 - 180;
    }

    tile2lat(y: number, z: number) {
        return y / Math.pow(2.0, (z + 1)) * 360.0 - 90;
    }
}

export default TileUtil4326;
