var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "crypto-js"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const crypto_js_1 = __importDefault(require("crypto-js"));
    class CryptoUtil {
        static Encrypt(dataStr, key) {
            return crypto_js_1.default.AES.encrypt(dataStr, key).toString();
        }
        static Decrypt(ciphertext, key) {
            let bytes = crypto_js_1.default.AES.decrypt(ciphertext, key);
            return bytes.toString(crypto_js_1.default.enc.Utf8);
        }
    }
    exports.default = CryptoUtil;
});
