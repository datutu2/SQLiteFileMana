// @ts-ignore
import rs from 'jsrsasign';

let RSATool = {
    /**
     * 获取秘钥
     * @returns {{priKey: *, pubKey: *}}
     */
    createKey(b = 2048) {
        let rsaKeypair = rs.KEYUTIL.generateKeypair('RSA', b);
        // 密钥对象获取 pem 格式的密钥
        let publicDer:string = rs.KEYUTIL.getPEM(rsaKeypair.pubKeyObj);
        let privateDer:string = rs.KEYUTIL.getPEM(rsaKeypair.prvKeyObj, 'PKCS8PRV');
        return {privateDer, publicDer};
    },

    encrypt(message: string, pubKey: string) {
        let pub = rs.KEYUTIL.getKey(pubKey);
        // @ts-ignore
        return rs.KJUR.crypto.Cipher.encrypt(message, pub, null);
    },

    decrypt(encryptTxt: string, privateK: string) {
        let pub = rs.KEYUTIL.getKey(privateK);
        // @ts-ignore
        return rs.KJUR.crypto.Cipher.decrypt(encryptTxt, pub, null);
    }
};

export default RSATool;
