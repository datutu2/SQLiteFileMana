var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "jsrsasign"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const jsrsasign_1 = __importDefault(require("jsrsasign"));
    let RSATool = {
        createKey(b = 2048) {
            let rsaKeypair = jsrsasign_1.default.KEYUTIL.generateKeypair('RSA', b);
            let publicDer = jsrsasign_1.default.KEYUTIL.getPEM(rsaKeypair.pubKeyObj);
            let privateDer = jsrsasign_1.default.KEYUTIL.getPEM(rsaKeypair.prvKeyObj, 'PKCS8PRV');
            return { privateDer, publicDer };
        },
        encrypt(message, pubKey) {
            let pub = jsrsasign_1.default.KEYUTIL.getKey(pubKey);
            return jsrsasign_1.default.KJUR.crypto.Cipher.encrypt(message, pub, null);
        },
        decrypt(encryptTxt, privateK) {
            let pub = jsrsasign_1.default.KEYUTIL.getKey(privateK);
            return jsrsasign_1.default.KJUR.crypto.Cipher.decrypt(encryptTxt, pub, null);
        }
    };
    exports.default = RSATool;
});
