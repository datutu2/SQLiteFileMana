/****************************************************************************
 名称：常用工具函数集合
 作者：冯功耀
 最后修改时间：2021 年 3 月 13 日
 ****************************************************************************/
import ArrTool from './Source/ArrTool';
import awaitWrap from './Source/AsyncTool';
import BOMTool from './Source/BOMTool';
import ColorTool from './Source/ColorTool';
import DateTool from './Source/DateTool';
import GISTool from './Source/GISTool';
import HTTPTool from './Source/HTTPTool';
import MathTool from './Source/MathTool';
import SafeTool from './Source/SafeTool';
import StringTool from './Source/StringTool';
import Util from './Source/Utils';

let YaoDo = {
    ArrTool,
    awaitWrap,
    BOMTool,
    ColorTool,
    DateTool,
    GISTool,
    HTTPTool,
    MathTool,
    SafeTool,
    StringTool,
    Util
};


export default YaoDo;
