var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./Source/ArrTool", "./Source/AsyncTool", "./Source/BOMTool", "./Source/ColorTool", "./Source/DateTool", "./Source/GISTool", "./Source/HTTPTool", "./Source/MathTool", "./Source/SafeTool", "./Source/StringTool", "./Source/Utils"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const ArrTool_1 = __importDefault(require("./Source/ArrTool"));
    const AsyncTool_1 = __importDefault(require("./Source/AsyncTool"));
    const BOMTool_1 = __importDefault(require("./Source/BOMTool"));
    const ColorTool_1 = __importDefault(require("./Source/ColorTool"));
    const DateTool_1 = __importDefault(require("./Source/DateTool"));
    const GISTool_1 = __importDefault(require("./Source/GISTool"));
    const HTTPTool_1 = __importDefault(require("./Source/HTTPTool"));
    const MathTool_1 = __importDefault(require("./Source/MathTool"));
    const SafeTool_1 = __importDefault(require("./Source/SafeTool"));
    const StringTool_1 = __importDefault(require("./Source/StringTool"));
    const Utils_1 = __importDefault(require("./Source/Utils"));
    let YaoDo = {
        ArrTool: ArrTool_1.default,
        awaitWrap: AsyncTool_1.default,
        BOMTool: BOMTool_1.default,
        ColorTool: ColorTool_1.default,
        DateTool: DateTool_1.default,
        GISTool: GISTool_1.default,
        HTTPTool: HTTPTool_1.default,
        MathTool: MathTool_1.default,
        SafeTool: SafeTool_1.default,
        StringTool: StringTool_1.default,
        Util: Utils_1.default
    };
    exports.default = YaoDo;
});
