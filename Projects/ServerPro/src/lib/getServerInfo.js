var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "os"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const os_1 = __importDefault(require("os"));
    const { machineId, machineIdSync } = require('node-machine-id');
    function getServerInfo() {
        return {
            'arch': os_1.default.arch(),
            'PCName': os_1.default.hostname(),
            'memorySize': Math.ceil(os_1.default.totalmem() / 1024 / 1024 / 1024) + 'GB',
            'operatingSystem': os_1.default.type(),
            'CPU': os_1.default.cpus()[0].model + ' X ' + os_1.default.cpus().length,
            'machineId': machineIdSync({ original: true })
        };
    }
    exports.default = getServerInfo;
});
