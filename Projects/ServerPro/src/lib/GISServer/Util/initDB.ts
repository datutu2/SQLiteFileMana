import DBTool from '../../../com/DBTool/DBTool';

async function initDB(DBName: string) {
    if (!await DBTool.HasDB(DBName)) {
        let dbTool = new DBTool(DBName);
        await dbTool.connect();
        await dbTool.close();
    }
}

export default initDB;
