import DBConnectTool from '../../../com/DBTool/DBConnectTool';
import DBTool from '../../../com/DBTool/DBTool';

async function readFromDB({DBName, relativePath, err, buffer, dataSource}: any) {
    if (!buffer) {
        let file;

        if (await DBTool.HasDB(DBName)) {
            let dbTool = await DBConnectTool.openDB(DBName);
            file = await dbTool.getFileByFullPath(relativePath);
        }

        if (file) {
            err = null;
            buffer = file.file_data;
            dataSource = 'DB';
        }
    }

    return [err, buffer, dataSource];
}

export default readFromDB;
