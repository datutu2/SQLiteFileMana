var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "../../../com/DBTool/DBConnectTool", "../../../com/DBTool/DBTool"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const DBConnectTool_1 = __importDefault(require("../../../com/DBTool/DBConnectTool"));
    const DBTool_1 = __importDefault(require("../../../com/DBTool/DBTool"));
    async function readFromDB({ DBName, relativePath, err, buffer, dataSource }) {
        if (!buffer) {
            let file;
            if (await DBTool_1.default.HasDB(DBName)) {
                let dbTool = await DBConnectTool_1.default.openDB(DBName);
                file = await dbTool.getFileByFullPath(relativePath);
            }
            if (file) {
                err = null;
                buffer = file.file_data;
                dataSource = 'DB';
            }
        }
        return [err, buffer, dataSource];
    }
    exports.default = readFromDB;
});
