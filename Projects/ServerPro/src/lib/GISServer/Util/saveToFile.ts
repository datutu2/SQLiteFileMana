import fsPromises from 'fs-promise';
import path from 'path';
import fs from 'fs';

async function saveToFile(fileSavePath: string, buffer: Buffer) {
    if (fileSavePath && buffer) {
        await fsPromises.ensureDir(path.dirname(fileSavePath));
        fs.writeFileSync(fileSavePath, buffer);
    }
}

export default saveToFile;
