var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "../../../com/DBTool/DBTool"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const DBTool_1 = __importDefault(require("../../../com/DBTool/DBTool"));
    async function initDB(DBName) {
        if (!await DBTool_1.default.HasDB(DBName)) {
            let dbTool = new DBTool_1.default(DBName);
            await dbTool.connect();
            await dbTool.close();
        }
    }
    exports.default = initDB;
});
