import path from 'path';
import zlibPromise from '../../../lib/FSTool/zlibPromise';
import DBConnectTool from '../../../com/DBTool/DBConnectTool';

async function saveToDB({DBName, relativePath, buffer}: any, needCompress = false) {
    if (DBName && relativePath && buffer) {
        let dbTool = await DBConnectTool.openDB(DBName);

        buffer = needCompress ? await zlibPromise.zip(buffer) : buffer;
        await dbTool.insertData([{
            relativePath: relativePath,
            fileName: path.basename(relativePath),
            buffer: buffer,
            compressType: needCompress ? 'gzip' : '',
            md5: require('crypto').createHash('md5').update(buffer).digest('hex'),
            size: buffer.length
        }], null, 'large', false);
    }
}

export default saveToDB;
