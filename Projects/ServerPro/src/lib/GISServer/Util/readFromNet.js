var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "axios"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const axios_1 = __importDefault(require("axios"));
    async function readFromNet({ url, headers, err, buffer, dataSource }) {
        if (!buffer) {
            await axios_1.default.get(url, {
                headers: headers || {},
                responseType: 'arraybuffer'
            }).then((res) => {
                dataSource = 'Net';
                buffer = res.data;
                err = null;
            }).catch((reqErr) => {
                err = reqErr;
            });
        }
        return [err, buffer, dataSource];
    }
    exports.default = readFromNet;
});
