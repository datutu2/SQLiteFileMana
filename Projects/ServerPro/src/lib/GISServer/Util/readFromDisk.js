var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "../../FSTool/readFileAsync"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const readFileAsync_1 = __importDefault(require("../../FSTool/readFileAsync"));
    async function readFromDisk({ fileSavePath, err, buffer, dataSource }) {
        if (!buffer) {
            [err, buffer] = await (0, readFileAsync_1.default)(fileSavePath);
            dataSource = 'Disk';
        }
        return [err, buffer, dataSource];
    }
    exports.default = readFromDisk;
});
