var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "path", "../../../lib/FSTool/zlibPromise", "../../../com/DBTool/DBConnectTool"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const path_1 = __importDefault(require("path"));
    const zlibPromise_1 = __importDefault(require("../../../lib/FSTool/zlibPromise"));
    const DBConnectTool_1 = __importDefault(require("../../../com/DBTool/DBConnectTool"));
    async function saveToDB({ DBName, relativePath, buffer }, needCompress = false) {
        if (DBName && relativePath && buffer) {
            let dbTool = await DBConnectTool_1.default.openDB(DBName);
            buffer = needCompress ? await zlibPromise_1.default.zip(buffer) : buffer;
            await dbTool.insertData([{
                    relativePath: relativePath,
                    fileName: path_1.default.basename(relativePath),
                    buffer: buffer,
                    compressType: needCompress ? 'gzip' : '',
                    md5: require('crypto').createHash('md5').update(buffer).digest('hex'),
                    size: buffer.length
                }], null, 'large', false);
        }
    }
    exports.default = saveToDB;
});
