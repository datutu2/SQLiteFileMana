var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "fs-promise", "path", "fs"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const fs_promise_1 = __importDefault(require("fs-promise"));
    const path_1 = __importDefault(require("path"));
    const fs_1 = __importDefault(require("fs"));
    async function saveToFile(fileSavePath, buffer) {
        if (fileSavePath && buffer) {
            await fs_promise_1.default.ensureDir(path_1.default.dirname(fileSavePath));
            fs_1.default.writeFileSync(fileSavePath, buffer);
        }
    }
    exports.default = saveToFile;
});
