var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "path", "../../../com/configTool", "../Util/saveToDB", "../Util/saveToFile", "../Util/readFromDB", "../Util/readFromNet", "../Util/initDB"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const path_1 = __importDefault(require("path"));
    const configTool_1 = __importDefault(require("../../../com/configTool"));
    const saveToDB_1 = __importDefault(require("../Util/saveToDB"));
    const saveToFile_1 = __importDefault(require("../Util/saveToFile"));
    const readFromDB_1 = __importDefault(require("../Util/readFromDB"));
    const readFromNet_1 = __importDefault(require("../Util/readFromNet"));
    const initDB_1 = __importDefault(require("../Util/initDB"));
    let Loading = true;
    let DBName = 'tile.openstreetmap.org';
    let baseUrlList = ['https://tile.openstreetmap.org', 'https://a.tile.openstreetmap.org', 'https://b.tile.openstreetmap.org', 'https://c.tile.openstreetmap.org'];
    let headers = ['Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.5 (KHTML, like Gecko) Chrome/4.0.249.0 Safari/532.5', 'Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.9 (KHTML, like Gecko) Chrome/5.0.310.0 Safari/532.9', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.7 (KHTML, like Gecko) Chrome/7.0.514.0 Safari/534.7', 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Chrome/9.0.601.0 Safari/534.14', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Chrome/10.0.601.0 Safari/534.14', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.20 (KHTML, like Gecko) Chrome/11.0.672.2 Safari/534.20", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.27 (KHTML, like Gecko) Chrome/12.0.712.0 Safari/534.27', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.24 Safari/535.1'];
    let OSMTile = {
        async init() {
            if (Loading) {
                Loading = false;
                await configTool_1.default.init();
                configTool_1.default.preservableONDB() && await (0, initDB_1.default)(DBName);
            }
        },
        async getFileBuffer(z, x, y) {
            await this.init();
            const header = headers[Math.floor(Math.random() * headers.length)];
            const relativePath = `\\${z}\\${x}\\${y}.png`;
            const url = baseUrlList[Math.floor(Math.random() * baseUrlList.length)] + `\\${z}\\${x}\\${y}.png`;
            const fileSavePath = path_1.default.join(configTool_1.default.appBasePath, `temp/fileOut/tile.openstreetmap.org`, relativePath);
            let err, buffer, dataSource;
            [err, buffer, dataSource] = await (0, readFromDB_1.default)({ DBName, relativePath, err, buffer, dataSource });
            console.log(url, { 'User-Agent': header });
            [err, buffer, dataSource] = await (0, readFromNet_1.default)({ url, headers: { 'User-Agent': header }, err, buffer, dataSource });
            dataSource !== 'DB' && configTool_1.default.preservableONDB() && (0, saveToDB_1.default)({ DBName, relativePath, buffer }).then();
            dataSource === 'Net' && configTool_1.default.preservableONFile() && (0, saveToFile_1.default)(fileSavePath, buffer).then();
            return [err, buffer, dataSource];
        }
    };
    exports.default = OSMTile;
});
