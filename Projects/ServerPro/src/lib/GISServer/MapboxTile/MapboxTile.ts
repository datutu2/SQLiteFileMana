import path from 'path';
import configTool from '../../../com/configTool';
import readFromDB from '../Util/readFromDB';
import readFromNet from '../Util/readFromNet';
import initDB from '../Util/initDB';
import saveToDB from '../Util/saveToDB';
import saveToFile from '../Util/saveToFile';
import readFromDisk from '../Util/readFromDisk';


let Loading = true;
const DBName = 'api.mapbox.com';
const satelliteUrl = 'https://api.mapbox.com/v4/mapbox.satellite';
const terrainVectorUrl = 'https://api.mapbox.com/v4/mapbox.mapbox-terrain-v2';
const terrainRGBUrl = 'https://api.mapbox.com/v4/mapbox.terrain-rgb';
// 高德地图标注
const appMapLabelUrl = 'http://webst02.is.autonavi.com/appmaptile?x={x}&y={y}&z={z}&lang=zh_cn&size=1&scale=1&style=8';

let MapboxTile = {
    async init() {
        if (Loading) {
            Loading = false;
            await configTool.init();
            configTool.preservableONDB() && await initDB(DBName);
        }
    },
    // 获取影像瓦片
    async getSatelliteFileBuffer(z: number, x: number, y: number) {
        await this.init();
        let err, buffer, dataSource;
        if (z > 22) {
            err = {msg: 'Zoom level must be between 0-22.'};
            return [err, buffer, dataSource];
        }

        // 从网络获取，并持久化存储
        const url = `${satelliteUrl}\\${z}\\${x}\\${y}.webp?access_token=${configTool.config.MapBox.access_token}`;

        const relativePath = `\\v4\\mapbox.satellite\\${z}\\${x}\\${y}.webp`;
        const fileSavePath = path.join(configTool.appBasePath, `temp/fileOut/api.mapbox.com`, relativePath);

        // 从本地文件夹中获取文件
        [err, buffer, dataSource] = await readFromDisk({fileSavePath, err, buffer, dataSource});

        // 从数据库中获取文件
        [err, buffer, dataSource] = await readFromDB({DBName, relativePath, err, buffer, dataSource});

        // 从网络获取，并持久化存储
        [err, buffer, dataSource] = await readFromNet({url, headers: configTool.config.MapBox.header, err, buffer, dataSource});

        dataSource !== 'DB' && configTool.preservableONDB() && saveToDB({DBName, relativePath, buffer}).then();
        dataSource === 'Net' && configTool.preservableONFile() && saveToFile(fileSavePath, buffer).then();

        return [err, buffer, dataSource];
    },
    // 获取地形颜色瓦片
    async getTerrainRGBFileBuffer(z: number, x: number, y: number) {
        await this.init();
        let err, buffer, dataSource;
        if (z > 22) {
            err = {msg: 'Zoom level must be between 0-22.'};
            return [err, buffer, dataSource];
        }

        let url = `${terrainRGBUrl}\\${z}\\${x}\\${y}@2x.pngraw?access_token=` + configTool.config.MapBox.access_token;
        let relativePath = `\\v4\\mapbox.terrain-rgb\\${z}\\${x}\\${y}.png`;
        let fileSavePath = path.join(configTool.appBasePath, `temp/fileOut/api.mapbox.com`, relativePath);

        // 从本地文件夹中获取文件
        [err, buffer, dataSource] = await readFromDisk({fileSavePath, err, buffer, dataSource});

        // 从数据库中获取文件
        [err, buffer, dataSource] = await readFromDB({DBName, relativePath, err, buffer, dataSource});

        // 从网络获取，并持久化存储
        [err, buffer, dataSource] = await readFromNet({url, headers: configTool.config.MapBox.header, err, buffer, dataSource});


        dataSource !== 'DB' && configTool.preservableONDB() && saveToDB({DBName, relativePath, buffer}).then();
        dataSource === 'Net' && configTool.preservableONFile() && saveToFile(fileSavePath, buffer).then();

        return [err, buffer, dataSource];
    },
    async getAppMapLabelFileBuffer(z: number, x: number, y: number) {
        await this.init();
        let err, buffer, dataSource;
        if (z > 22) {
            err = {msg: 'Zoom level must be between 0-22.'};
            return [err, buffer, dataSource];
        }

        let url =appMapLabelUrl.replace(`{z}`,String(z)).replace(`{x}`,String(x)).replace(`{y}`,String(y));
        let relativePath = `\\${z}\\${x}\\${y}.png`;
        let fileSavePath = path.join(configTool.appBasePath, `temp/fileOut/appMapLabel`, relativePath);

        // 从本地文件夹中获取文件
        [err, buffer, dataSource] = await readFromDisk({fileSavePath, err, buffer, dataSource});

        // 从数据库中获取文件
        [err, buffer, dataSource] = await readFromDB({DBName, relativePath, err, buffer, dataSource});

        // 从网络获取，并持久化存储
        [err, buffer, dataSource] = await readFromNet({url, headers: {}, err, buffer, dataSource});


        dataSource !== 'DB' && configTool.preservableONDB() && saveToDB({DBName, relativePath, buffer}).then();
        dataSource === 'Net' && configTool.preservableONFile() && saveToFile(fileSavePath, buffer).then();

        return [err, buffer, dataSource];
    },
};

export default MapboxTile;
