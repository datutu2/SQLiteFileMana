var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "path", "../../../com/configTool", "../Util/readFromDB", "../Util/readFromNet", "../Util/initDB", "../Util/saveToDB", "../Util/saveToFile", "../Util/readFromDisk"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const path_1 = __importDefault(require("path"));
    const configTool_1 = __importDefault(require("../../../com/configTool"));
    const readFromDB_1 = __importDefault(require("../Util/readFromDB"));
    const readFromNet_1 = __importDefault(require("../Util/readFromNet"));
    const initDB_1 = __importDefault(require("../Util/initDB"));
    const saveToDB_1 = __importDefault(require("../Util/saveToDB"));
    const saveToFile_1 = __importDefault(require("../Util/saveToFile"));
    const readFromDisk_1 = __importDefault(require("../Util/readFromDisk"));
    let Loading = true;
    const DBName = 'api.mapbox.com';
    const satelliteUrl = 'https://api.mapbox.com/v4/mapbox.satellite';
    const terrainVectorUrl = 'https://api.mapbox.com/v4/mapbox.mapbox-terrain-v2';
    const terrainRGBUrl = 'https://api.mapbox.com/v4/mapbox.terrain-rgb';
    const appMapLabelUrl = 'http://webst02.is.autonavi.com/appmaptile?x={x}&y={y}&z={z}&lang=zh_cn&size=1&scale=1&style=8';
    let MapboxTile = {
        async init() {
            if (Loading) {
                Loading = false;
                await configTool_1.default.init();
                configTool_1.default.preservableONDB() && await (0, initDB_1.default)(DBName);
            }
        },
        async getSatelliteFileBuffer(z, x, y) {
            await this.init();
            let err, buffer, dataSource;
            if (z > 22) {
                err = { msg: 'Zoom level must be between 0-22.' };
                return [err, buffer, dataSource];
            }
            const url = `${satelliteUrl}\\${z}\\${x}\\${y}.webp?access_token=${configTool_1.default.config.MapBox.access_token}`;
            const relativePath = `\\v4\\mapbox.satellite\\${z}\\${x}\\${y}.webp`;
            const fileSavePath = path_1.default.join(configTool_1.default.appBasePath, `temp/fileOut/api.mapbox.com`, relativePath);
            [err, buffer, dataSource] = await (0, readFromDisk_1.default)({ fileSavePath, err, buffer, dataSource });
            [err, buffer, dataSource] = await (0, readFromDB_1.default)({ DBName, relativePath, err, buffer, dataSource });
            [err, buffer, dataSource] = await (0, readFromNet_1.default)({ url, headers: configTool_1.default.config.MapBox.header, err, buffer, dataSource });
            dataSource !== 'DB' && configTool_1.default.preservableONDB() && (0, saveToDB_1.default)({ DBName, relativePath, buffer }).then();
            dataSource === 'Net' && configTool_1.default.preservableONFile() && (0, saveToFile_1.default)(fileSavePath, buffer).then();
            return [err, buffer, dataSource];
        },
        async getTerrainRGBFileBuffer(z, x, y) {
            await this.init();
            let err, buffer, dataSource;
            if (z > 22) {
                err = { msg: 'Zoom level must be between 0-22.' };
                return [err, buffer, dataSource];
            }
            let url = `${terrainRGBUrl}\\${z}\\${x}\\${y}@2x.pngraw?access_token=` + configTool_1.default.config.MapBox.access_token;
            let relativePath = `\\v4\\mapbox.terrain-rgb\\${z}\\${x}\\${y}.png`;
            let fileSavePath = path_1.default.join(configTool_1.default.appBasePath, `temp/fileOut/api.mapbox.com`, relativePath);
            [err, buffer, dataSource] = await (0, readFromDisk_1.default)({ fileSavePath, err, buffer, dataSource });
            [err, buffer, dataSource] = await (0, readFromDB_1.default)({ DBName, relativePath, err, buffer, dataSource });
            [err, buffer, dataSource] = await (0, readFromNet_1.default)({ url, headers: configTool_1.default.config.MapBox.header, err, buffer, dataSource });
            dataSource !== 'DB' && configTool_1.default.preservableONDB() && (0, saveToDB_1.default)({ DBName, relativePath, buffer }).then();
            dataSource === 'Net' && configTool_1.default.preservableONFile() && (0, saveToFile_1.default)(fileSavePath, buffer).then();
            return [err, buffer, dataSource];
        },
        async getAppMapLabelFileBuffer(z, x, y) {
            await this.init();
            let err, buffer, dataSource;
            if (z > 22) {
                err = { msg: 'Zoom level must be between 0-22.' };
                return [err, buffer, dataSource];
            }
            let url = appMapLabelUrl.replace(`{z}`, String(z)).replace(`{x}`, String(x)).replace(`{y}`, String(y));
            let relativePath = `\\${z}\\${x}\\${y}.png`;
            let fileSavePath = path_1.default.join(configTool_1.default.appBasePath, `temp/fileOut/appMapLabel`, relativePath);
            [err, buffer, dataSource] = await (0, readFromDisk_1.default)({ fileSavePath, err, buffer, dataSource });
            [err, buffer, dataSource] = await (0, readFromDB_1.default)({ DBName, relativePath, err, buffer, dataSource });
            [err, buffer, dataSource] = await (0, readFromNet_1.default)({ url, headers: {}, err, buffer, dataSource });
            dataSource !== 'DB' && configTool_1.default.preservableONDB() && (0, saveToDB_1.default)({ DBName, relativePath, buffer }).then();
            dataSource === 'Net' && configTool_1.default.preservableONFile() && (0, saveToFile_1.default)(fileSavePath, buffer).then();
            return [err, buffer, dataSource];
        },
    };
    exports.default = MapboxTile;
});
