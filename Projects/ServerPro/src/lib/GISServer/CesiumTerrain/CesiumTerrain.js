var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "path", "../../../ext/turf", "axios", "../../../com/configTool", "../Util/readFromDB", "../Util/readFromNet", "../Util/saveToDB", "../Util/saveToFile", "../Util/initDB", "../../../lib/awaitWrap", "../../../lib/TileTool/TileUtil4326", "../../TileTool/TileSet"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const path_1 = __importDefault(require("path"));
    const turf_1 = __importDefault(require("../../../ext/turf"));
    const axios_1 = __importDefault(require("axios"));
    const configTool_1 = __importDefault(require("../../../com/configTool"));
    const readFromDB_1 = __importDefault(require("../Util/readFromDB"));
    const readFromNet_1 = __importDefault(require("../Util/readFromNet"));
    const saveToDB_1 = __importDefault(require("../Util/saveToDB"));
    const saveToFile_1 = __importDefault(require("../Util/saveToFile"));
    const initDB_1 = __importDefault(require("../Util/initDB"));
    const awaitWrap_1 = __importDefault(require("../../../lib/awaitWrap"));
    const TileUtil4326_1 = __importDefault(require("../../../lib/TileTool/TileUtil4326"));
    const TileSet_1 = __importDefault(require("../../TileTool/TileSet"));
    let loading = false;
    const DBName = 'CesiumWorldTerrain';
    const baseUrl = 'https://assets.ion.cesium.com/asset_depot/1/CesiumWorldTerrain/v1.2/';
    const ext = '?extensions=octvertexnormals-watermask-metadata&v=1.2.0';
    let Authorization = '';
    let CesiumTerrain = {
        async init() {
            await configTool_1.default.init();
            configTool_1.default.preservableONDB() && await (0, initDB_1.default)(DBName);
        },
        async getFileBuffer(url) {
            await this.init();
            if (loading) {
                await new Promise((resolve) => {
                    let timer = setInterval(() => {
                        if (!loading) {
                            clearInterval(timer);
                            resolve(null);
                        }
                    }, 100);
                });
            }
            if (!Authorization) {
                loading = true;
                let [err, data] = await (0, awaitWrap_1.default)(axios_1.default.get('https://api.cesium.com/v1/assets/1/endpoint?access_token=' + configTool_1.default.config.Cesium.IonAccessToken));
                loading = false;
                if (data) {
                    data = data.data;
                    Authorization = 'Bearer ' + data.accessToken;
                    console.log('获取令牌成功', data.accessToken);
                }
                else {
                    console.log('获取令牌失败');
                }
            }
            let err, buffer, dataSource;
            let headers = { 'Authorization': Authorization };
            let relativePath = '\\1\\' + url;
            let fileSavePath = path_1.default.join(configTool_1.default.appBasePath, `temp/fileOut/assets.cesium.com`, relativePath);
            [err, buffer, dataSource] = await (0, readFromDB_1.default)({ DBName, relativePath, err, buffer, dataSource });
            [err, buffer, dataSource] = await (0, readFromNet_1.default)({ url: baseUrl + url + ext, headers, err, buffer, dataSource });
            if (err?.state === 404) {
            }
            else if (err?.state === 503) {
                console.log('请求过于频繁，服务器拒绝响应');
            }
            else if (err?.state === 401) {
                console.log(err);
                console.log('重置 Cesium 地形资源的 令牌');
                Authorization = '';
            }
            buffer && dataSource !== 'DB' && configTool_1.default.preservableONDB() && (0, saveToDB_1.default)({ DBName, relativePath, buffer }, true).then();
            buffer && dataSource === 'Net' && configTool_1.default.preservableONFile() && (0, saveToFile_1.default)(fileSavePath, buffer).then();
            return [err, buffer];
        },
        async getTileListByGeoJson(zoom, geoJson, layerJson) {
            await this.init();
            const tileUtil4326 = new TileUtil4326_1.default();
            if (!geoJson) {
                geoJson = {
                    'type': 'Feature',
                    'properties': {},
                    'geometry': {
                        'type': 'Polygon',
                        'coordinates': [[
                                [-180, -90],
                                [180, -90],
                                [180, 90],
                                [-180, 90],
                                [-180, -90]
                            ]]
                    }
                };
            }
            if (!layerJson) {
                const [err, buffer] = await CesiumTerrain.getFileBuffer('layer.json');
                layerJson = buffer ? JSON.parse(buffer.toString()) : null;
                if (err || layerJson === null) {
                    return { tileList: [], boxList: [] };
                }
            }
            const dataRanges = layerJson.available;
            const tileSet = new TileSet_1.default();
            const boxList = [];
            const areaRangeList = dataRanges[zoom];
            const effectiveAreaRangeList = [];
            for (let i = 0; i < areaRangeList?.length; i++) {
                const areaRange = areaRangeList[i];
                const minTile = tileUtil4326.tileXYZToRectanglePolygon(areaRange.startX, areaRange.startY, zoom);
                const maxTile = tileUtil4326.tileXYZToRectanglePolygon(areaRange.endX, areaRange.endY, zoom);
                const box = turf_1.default.bboxPolygon(turf_1.default.bbox(turf_1.default.featureCollection([minTile, maxTile])));
                boxList.push(box);
                if (!turf_1.default.booleanDisjoint(geoJson, box)) {
                    effectiveAreaRangeList.push(areaRange);
                    for (let x = areaRange.startX; x <= areaRange.endX; x++) {
                        for (let y = areaRange.startY; y <= areaRange.endY; y++) {
                            tileSet.add(x, y, zoom);
                        }
                    }
                }
            }
            return { tileList: tileSet.getAll(), boxList, effectiveAreaRangeList };
        }
    };
    exports.default = CesiumTerrain;
});
