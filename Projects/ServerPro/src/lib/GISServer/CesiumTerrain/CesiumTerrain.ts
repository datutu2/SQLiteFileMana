import path from 'path';
import turf from '../../../ext/turf';
import axios from 'axios';
import configTool from '../../../com/configTool';
import readFromDB from '../Util/readFromDB';
import readFromNet from '../Util/readFromNet';
import saveToDB from '../Util/saveToDB';
import saveToFile from '../Util/saveToFile';
import initDB from '../Util/initDB';
import awaitWrap from '../../../lib/awaitWrap';
import TileUtil4326 from '../../../lib/TileTool/TileUtil4326';
import TileSet from '../../TileTool/TileSet';


let loading = false;
const DBName = 'CesiumWorldTerrain';
const baseUrl = 'https://assets.ion.cesium.com/asset_depot/1/CesiumWorldTerrain/v1.2/';
const ext = '?extensions=octvertexnormals-watermask-metadata&v=1.2.0';
let Authorization = '';

let CesiumTerrain = {
    async init() {
        await configTool.init();
        configTool.preservableONDB() && await initDB(DBName);
    },
    async getFileBuffer(url: string) {
        await this.init();
        if (loading) {
            await new Promise((resolve) => {
                let timer = setInterval(() => {
                    if (!loading) {
                        clearInterval(timer);
                        resolve(null);
                    }
                }, 100);
            });
        }


        if (!Authorization) {
            loading = true;
            let [err, data] = await awaitWrap(axios.get('https://api.cesium.com/v1/assets/1/endpoint?access_token=' + configTool.config.Cesium.IonAccessToken));
            loading = false;
            if (data) {
                data = data.data;
                Authorization = 'Bearer ' + data.accessToken;
                console.log('获取令牌成功', data.accessToken);
            } else {
                console.log('获取令牌失败');
            }
        }
        let err, buffer, dataSource;
        let headers = { 'Authorization': Authorization };

        let relativePath = '\\1\\' + url;
        let fileSavePath = path.join(configTool.appBasePath, `temp/fileOut/assets.cesium.com`, relativePath);

        // 从数据库中获取文件
        [err, buffer, dataSource] = await readFromDB({ DBName, relativePath, err, buffer, dataSource });

        // 从网络获取，并持久化存储
        [err, buffer, dataSource] = await readFromNet({ url: baseUrl + url + ext, headers, err, buffer, dataSource });

        if (err?.state === 404) {
        } else if (err?.state === 503) {
            console.log('请求过于频繁，服务器拒绝响应');
        } else if (err?.state === 401) {
            console.log(err);
            console.log('重置 Cesium 地形资源的 令牌');
            Authorization = '';
        }

        buffer && dataSource !== 'DB' && configTool.preservableONDB() && saveToDB({ DBName, relativePath, buffer }, true).then();
        buffer && dataSource === 'Net' && configTool.preservableONFile() && saveToFile(fileSavePath, buffer).then();

        return [err, buffer];
    },
    async getTileListByGeoJson(zoom: number, geoJson: any, layerJson: any) {
        await this.init();
        const tileUtil4326 = new TileUtil4326();

        if (!geoJson) {
            geoJson = {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'Polygon',
                    'coordinates': [[
                        [-180, -90],
                        [180, -90],
                        [180, 90],
                        [-180, 90],
                        [-180, -90]
                    ]]
                }
            };
        }
        if (!layerJson) {
            const [err, buffer] = await CesiumTerrain.getFileBuffer('layer.json');
            layerJson = buffer ? JSON.parse(buffer.toString()) : null;
            if (err || layerJson === null) {
                return { tileList: [], boxList: [] };
            }
        }


        const dataRanges = layerJson.available;

        const tileSet = new TileSet();
        const boxList = [];
        const areaRangeList = dataRanges[zoom];
        const effectiveAreaRangeList = [];
        for (let i = 0; i < areaRangeList?.length; i++) {
            const areaRange = areaRangeList[i];
            const minTile = tileUtil4326.tileXYZToRectanglePolygon(areaRange.startX, areaRange.startY, zoom);
            const maxTile = tileUtil4326.tileXYZToRectanglePolygon(areaRange.endX, areaRange.endY, zoom);
            const box = turf.bboxPolygon(turf.bbox(turf.featureCollection([minTile, maxTile])));
            boxList.push(box);
            if (!turf.booleanDisjoint(geoJson, box)) {
                effectiveAreaRangeList.push(areaRange);
                for (let x = areaRange.startX; x <= areaRange.endX; x++) {
                    for (let y = areaRange.startY; y <= areaRange.endY; y++) {
                        tileSet.add(x, y, zoom);
                    }
                }
            }
        }


        return { tileList: tileSet.getAll(), boxList, effectiveAreaRangeList };
    }
};


export default CesiumTerrain;

