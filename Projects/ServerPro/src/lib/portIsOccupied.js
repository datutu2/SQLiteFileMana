var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "net", "colors-console"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const net_1 = __importDefault(require("net"));
    const colors_console_1 = __importDefault(require("colors-console"));
    async function portIsOccupied(port, log = true) {
        const server = net_1.default.createServer().listen(port);
        return new Promise((resolve, reject) => {
            server.on('listening', () => {
                log && console.log((0, colors_console_1.default)('green', `您可以使用该端口： ${port}`));
                server.close();
                resolve(port);
            });
            server.on('error', async (err) => {
                if (err.code === 'EADDRINUSE') {
                    resolve(await portIsOccupied(port + 1, log));
                    log && console.log((0, colors_console_1.default)('red', `端口已被占用： ${port}`));
                }
                else {
                    reject(err);
                }
            });
        });
    }
    exports.default = portIsOccupied;
});
