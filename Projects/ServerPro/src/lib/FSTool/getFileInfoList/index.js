var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "../../../com/configTool", "./simpleFileInfoList", "./randomAsync", "./phasedFileInfoList", "./fiveThreadFileInfoList/index"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const configTool_1 = __importDefault(require("../../../com/configTool"));
    const simpleFileInfoList_1 = __importDefault(require("./simpleFileInfoList"));
    const randomAsync_1 = __importDefault(require("./randomAsync"));
    const phasedFileInfoList_1 = __importDefault(require("./phasedFileInfoList"));
    const index_1 = __importDefault(require("./fiveThreadFileInfoList/index"));
    async function getFileInfoList({ filePathList, mode, phasedFunc, progressFunc, rootPath }) {
        await configTool_1.default.init();
        filePathList = filePathList || [];
        if (!mode && filePathList.length < 5000) {
            mode = 'simple';
        }
        else if (!mode && filePathList.length < 10000) {
            mode = 'phased';
        }
        else if (!mode) {
            mode = 'fiveThread';
        }
        phasedFunc = phasedFunc || null;
        progressFunc = progressFunc || null;
        rootPath = rootPath || '';
        let fileInfoList = [];
        switch (mode) {
            case 'simple':
                fileInfoList = await (0, simpleFileInfoList_1.default)(filePathList, progressFunc, rootPath);
                break;
            case 'phased':
                fileInfoList = await (0, phasedFileInfoList_1.default)({ filePathList, phasedFunc, progressFunc, rootPath });
                break;
            case 'fiveThread':
                fileInfoList = await (0, index_1.default)(filePathList, phasedFunc, progressFunc, rootPath);
                break;
            case 'randomAsync':
                fileInfoList = await (0, randomAsync_1.default)({ filePathList, progressFunc, rootPath });
                break;
        }
        return fileInfoList;
    }
    exports.default = getFileInfoList;
});
