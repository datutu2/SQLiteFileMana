interface FileInfo {
    fullPath: string,           // 文件绝对路径
    relativePath: string,       // 文件相对路径
    fileName: string,           // 文件名
    buffer: Buffer | string,             // 文件内容
    compressType: string,       // 压缩类型
    md5: string,                // 文件 MD5
    size: number                // 文件大小
}


export default FileInfo;