var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "../calcFileInfo"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const calcFileInfo_1 = __importDefault(require("../calcFileInfo"));
    async function simpleFileInfoList(filePathList, progressFunc, rootPath) {
        let md5List = [];
        let allFileListCount = filePathList.length;
        let ProgressNumber = Math.floor(allFileListCount / 50) < 1 ? 1 : Math.floor(allFileListCount / 50);
        let reportProgress = 0;
        for (let i = 0; i < allFileListCount; i++) {
            let filePath = filePathList.pop();
            if (filePath) {
                let fileInfo = await (0, calcFileInfo_1.default)(filePath, rootPath);
                md5List.push(fileInfo);
                reportProgress++;
                if (reportProgress === ProgressNumber) {
                    reportProgress = 0;
                    typeof progressFunc === 'function' && progressFunc({
                        description: '【扫描 MD5】',
                        completed: i,
                        total: allFileListCount
                    });
                }
            }
        }
        typeof progressFunc === 'function' && progressFunc({ description: '【扫描 MD5】', completed: allFileListCount, total: allFileListCount });
        return md5List;
    }
    exports.default = simpleFileInfoList;
});
