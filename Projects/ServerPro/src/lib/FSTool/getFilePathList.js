(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "fdir"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const fdir_1 = require("fdir");
    async function getFilePathList(parentPath) {
        const api = new fdir_1.fdir().withFullPaths().crawl(parentPath);
        return await api.withPromise();
    }
    exports.default = getFilePathList;
});
