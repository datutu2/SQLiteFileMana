var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "fs"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const fs_1 = __importDefault(require("fs"));
    async function getPathInfo(path, calcMd5 = true) {
        return new Promise(function (resolve) {
            fs_1.default.stat(path, (err, stats) => {
                if (err) {
                    resolve(null);
                }
                else {
                    const info = {
                        isDirectory: !!stats && stats.isDirectory(),
                        size: stats.size / 1000,
                        md5: null
                    };
                    if (calcMd5 && !info.isDirectory) {
                        info.md5 = require('crypto').createHash('md5').update(fs_1.default.readFileSync(path)).digest('hex');
                    }
                    resolve(info);
                }
            });
        });
    }
    exports.default = getPathInfo;
});
