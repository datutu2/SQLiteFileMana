var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "fs"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const fs_1 = __importDefault(require("fs"));
    function deleteFile(path) {
        let result = true;
        try {
            fs_1.default.unlinkSync(path);
        }
        catch (e) {
            result = false;
            console.warn('文件删除失败', path);
        }
        return result;
    }
    exports.default = deleteFile;
});
