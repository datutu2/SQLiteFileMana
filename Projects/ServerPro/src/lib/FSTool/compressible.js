var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "path"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const path_1 = __importDefault(require("path"));
    function compressible(filePath) {
        let ext = path_1.default.extname(filePath).toLowerCase();
        let boolean = true;
        let fileExtList = [
            '.png',
            '.jpg',
            '.jpeg',
            '.zip',
            '.rar',
            '.mp3',
            '.mp4',
            '.avi',
            '.gif'
        ];
        fileExtList.forEach(e => e === ext && (boolean = false));
        return boolean;
    }
    exports.default = compressible;
});
