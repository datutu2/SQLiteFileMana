import fs from 'fs';

function readFileAsync(filePath: string, encoding?: any): Promise<[any, any]> {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, encoding, (err: any, data: Buffer) => {
            if (err) {
                resolve([err, null]);
            } else {
                resolve([null, data]);
            }
        });
    });
}


export default readFileAsync;
