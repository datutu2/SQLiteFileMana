var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "fs"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const fs_1 = __importDefault(require("fs"));
    function createDirectory(path) {
        const dirCache = {};
        const arr = path.replace(/\\/g, '/').split('/');
        let dir = arr[0];
        for (let i = 1; i < arr.length; i++) {
            if (!dirCache[dir] && !fs_1.default.existsSync(dir)) {
                dirCache[dir] = true;
                fs_1.default.mkdirSync(dir);
            }
            dir = dir + '/' + arr[i];
        }
    }
    exports.default = createDirectory;
});
