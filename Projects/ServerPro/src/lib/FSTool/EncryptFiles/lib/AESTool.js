(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const CryptoJS = require('crypto-js');
    const AESTool = {
        createKey() {
            const expect = 32;
            let str = Math.random().toString(36).substr(2);
            while (str.length < expect) {
                str += Math.random().toString(36).substr(2);
            }
            str = str.substr(0, 32);
            return str;
        },
        encrypt(data, secretKey) {
            const result = CryptoJS.AES.encrypt(data, CryptoJS.enc.Utf8.parse(secretKey), {
                iv: CryptoJS.enc.Utf8.parse('偏移量'),
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });
            return result.toString();
        },
        decrypt(cipher, secretKey) {
            const decrypted = CryptoJS.AES.decrypt(cipher, CryptoJS.enc.Utf8.parse(secretKey), {
                iv: CryptoJS.enc.Utf8.parse('偏移量'),
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });
            return CryptoJS.enc.Utf8.stringify(decrypted);
        }
    };
    exports.default = AESTool;
});
