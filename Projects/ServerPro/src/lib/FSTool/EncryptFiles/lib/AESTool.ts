const CryptoJS = require('crypto-js');


const AESTool = {
    createKey() {
        const expect = 32;
        let str = Math.random().toString(36).substr(2);
        while (str.length < expect) {
            str += Math.random().toString(36).substr(2);
        }
        str = str.substr(0, 32);

        return str;
    },

    encrypt(data: string, secretKey: string) {
        const result = CryptoJS.AES.encrypt(data, CryptoJS.enc.Utf8.parse(secretKey), {
            iv: CryptoJS.enc.Utf8.parse('偏移量'),
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });

        return result.toString();
    },

    decrypt(cipher: string, secretKey: string) {
        const decrypted = CryptoJS.AES.decrypt(cipher, CryptoJS.enc.Utf8.parse(secretKey), {
            iv: CryptoJS.enc.Utf8.parse('偏移量'),
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });

        return CryptoJS.enc.Utf8.stringify(decrypted);
    }
};

export default AESTool;
