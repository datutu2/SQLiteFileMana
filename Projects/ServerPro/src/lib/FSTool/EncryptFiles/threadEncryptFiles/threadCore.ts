import fs from 'fs';
import path from 'path';
import CryptoUtil from './CryptoUtil.min.js';
import createFile from '../../createFile';


process.on('message',
    async function ({ threadPathList, inputPath, outPutPath, passWorld }) {
        let ProgressNumber = 0;

        for (let i = 0; i < threadPathList.length; i++) {
            const filePath = threadPathList[i];
            const fileName = path.basename(filePath);
            const buffer = fs.readFileSync(filePath);
            const blob = new Blob([buffer]);
            const [encryptErr, encryptBlob] = await CryptoUtil.encryptByBlob(blob, passWorld, fileName, false);
            const encryptBuffer = Buffer.from(await encryptBlob.arrayBuffer());
            createFile(path.join(outPutPath, filePath.replace(inputPath, '')), encryptBuffer);

            ProgressNumber++;
            if (ProgressNumber === 25) {
                process?.send?.({ message: 'Progress', data: ProgressNumber });
                ProgressNumber = 0;
            }
        }

        process?.send?.({ message: 'Progress', data: ProgressNumber });
        process?.send?.({ message: 'Complete' });
    }
);

