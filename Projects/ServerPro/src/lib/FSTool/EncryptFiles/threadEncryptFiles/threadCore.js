var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "fs", "path", "./CryptoUtil.min.js", "../../createFile"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const fs_1 = __importDefault(require("fs"));
    const path_1 = __importDefault(require("path"));
    const CryptoUtil_min_js_1 = __importDefault(require("./CryptoUtil.min.js"));
    const createFile_1 = __importDefault(require("../../createFile"));
    process.on('message', async function ({ threadPathList, inputPath, outPutPath, passWorld }) {
        let ProgressNumber = 0;
        for (let i = 0; i < threadPathList.length; i++) {
            const filePath = threadPathList[i];
            const fileName = path_1.default.basename(filePath);
            const buffer = fs_1.default.readFileSync(filePath);
            const blob = new Blob([buffer]);
            const [encryptErr, encryptBlob] = await CryptoUtil_min_js_1.default.encryptByBlob(blob, passWorld, fileName, false);
            const encryptBuffer = Buffer.from(await encryptBlob.arrayBuffer());
            (0, createFile_1.default)(path_1.default.join(outPutPath, filePath.replace(inputPath, '')), encryptBuffer);
            ProgressNumber++;
            if (ProgressNumber === 25) {
                process?.send?.({ message: 'Progress', data: ProgressNumber });
                ProgressNumber = 0;
            }
        }
        process?.send?.({ message: 'Progress', data: ProgressNumber });
        process?.send?.({ message: 'Complete' });
    });
});
