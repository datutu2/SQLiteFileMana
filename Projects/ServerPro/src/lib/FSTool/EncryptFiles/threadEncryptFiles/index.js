var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "child_process", "path"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const child_process_1 = __importDefault(require("child_process"));
    const path_1 = __importDefault(require("path"));
    async function threadEncryptFiles(filePathList, inputPath, outPutPath, passWorld, progressFunc, threadNum = 5) {
        let optionObj = {
            index: 0,
            inputPath,
            outPutPath,
            passWorld,
            progressLength: filePathList.length
        };
        let threadRunNum = Number(threadNum) ? threadNum : 5;
        threadRunNum = threadRunNum < 0 ? 5 : threadRunNum;
        threadRunNum = threadRunNum > 10 ? 10 : threadRunNum;
        const batchDataList = group(filePathList, threadRunNum);
        const resData = await Promise.all(batchDataList.map(batchData => creatThread('线程1', batchData, progressFunc, optionObj)));
        return resData.flat(2);
    }
    function creatThread(threadName, threadPathList, progressFunc, optionObj = {}) {
        return new Promise(function (resolve) {
            let n = child_process_1.default.fork(path_1.default.join(__dirname, './threadCore.js'));
            n.send({ threadName, threadPathList, inputPath: optionObj.inputPath, outPutPath: optionObj.outPutPath, passWorld: optionObj.passWorld });
            n.on('message', function (reData) {
                let message = reData.message;
                switch (message) {
                    case 'Progress':
                        {
                            optionObj.index += reData.data;
                            typeof progressFunc === 'function' && progressFunc({
                                status: 'Progress',
                                description: '【开启 5 进程加密文件】',
                                completed: optionObj.index,
                                total: optionObj.progressLength
                            });
                        }
                        break;
                    case 'Complete': {
                        resolve({ status: 'Complete' });
                        n.disconnect();
                    }
                }
            });
        });
    }
    function group(array, subNum) {
        let part = (array.length / subNum) + 1;
        let newArray = [];
        for (let i = 0; i < subNum; i++) {
            newArray.push(array.slice(i * part, (i + 1) * part));
        }
        return newArray;
    }
    exports.default = threadEncryptFiles;
});
