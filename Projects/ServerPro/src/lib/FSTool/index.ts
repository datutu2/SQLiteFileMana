import fs from 'fs';

import zlibPromise from './zlibPromise';
import calcFileInfo from './calcFileInfo';
import createFile from './createFile';
import createFileAsync from './createFileAsync';
import deleteFile from './deleteFile';
import getFilePathList from './getFilePathList';
import getPathInfo from './getPathInfo';
import getFileInfoList from './getFileInfoList/index';
import compressible from './compressible';
import pathSplit from './pathSplit';
import readFileAsync from './readFileAsync';

const Index = {
    zlibPromise: zlibPromise,
    calcFileInfo: calcFileInfo,
    compressible: compressible,
    deleteFile,
    getPathInfo,
    getFilePathList,
    getFileInfoList,
    createFile,
    createFileAsync,
    pathSplit,

    // 异步读取文件
    readFileAsync,
    // 同步读取文件
    readFileSync: fs.readFileSync,
    writeFileSync: fs.writeFileSync
};

export default Index;
