var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "zlib"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const zlib_1 = __importDefault(require("zlib"));
    const zlibPromise = {
        zip: async function (data) {
            return new Promise(function (resolve, reject) {
                zlib_1.default.gzip(data, function (err, buffer) {
                    resolve(buffer);
                });
            });
        },
        unzip: async function (buffer) {
            return new Promise(function (resolve, reject) {
                if (!buffer) {
                    resolve(null);
                }
                else {
                    zlib_1.default.unzip(buffer, function (err, buffer) {
                        resolve(buffer);
                    });
                }
            });
        }
    };
    exports.default = zlibPromise;
});
