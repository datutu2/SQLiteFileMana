var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "fs", "./zlibPromise", "./calcFileInfo", "./createFile", "./createFileAsync", "./deleteFile", "./getFilePathList", "./getPathInfo", "./getFileInfoList/index", "./compressible", "./pathSplit", "./readFileAsync"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const fs_1 = __importDefault(require("fs"));
    const zlibPromise_1 = __importDefault(require("./zlibPromise"));
    const calcFileInfo_1 = __importDefault(require("./calcFileInfo"));
    const createFile_1 = __importDefault(require("./createFile"));
    const createFileAsync_1 = __importDefault(require("./createFileAsync"));
    const deleteFile_1 = __importDefault(require("./deleteFile"));
    const getFilePathList_1 = __importDefault(require("./getFilePathList"));
    const getPathInfo_1 = __importDefault(require("./getPathInfo"));
    const index_1 = __importDefault(require("./getFileInfoList/index"));
    const compressible_1 = __importDefault(require("./compressible"));
    const pathSplit_1 = __importDefault(require("./pathSplit"));
    const readFileAsync_1 = __importDefault(require("./readFileAsync"));
    const Index = {
        zlibPromise: zlibPromise_1.default,
        calcFileInfo: calcFileInfo_1.default,
        compressible: compressible_1.default,
        deleteFile: deleteFile_1.default,
        getPathInfo: getPathInfo_1.default,
        getFilePathList: getFilePathList_1.default,
        getFileInfoList: index_1.default,
        createFile: createFile_1.default,
        createFileAsync: createFileAsync_1.default,
        pathSplit: pathSplit_1.default,
        readFileAsync: readFileAsync_1.default,
        readFileSync: fs_1.default.readFileSync,
        writeFileSync: fs_1.default.writeFileSync
    };
    exports.default = Index;
});
