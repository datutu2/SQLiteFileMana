var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "fs", "./compressible", "./zlibPromise"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const fs_1 = __importDefault(require("fs"));
    const compressible_1 = __importDefault(require("./compressible"));
    const zlibPromise_1 = __importDefault(require("./zlibPromise"));
    async function readFile(path) {
        return new Promise(function (resolve) {
            fs_1.default.readFile(path, function (error, date) {
                if (error) {
                    resolve(Buffer.from(''));
                }
                else {
                    resolve(date);
                }
            });
        });
    }
    async function calcFileInfo(fullPath, rootPath = '', compress = true) {
        let relativePath = fullPath.replace(rootPath, '');
        let fileName = fullPath.split('\\').slice(-1)[0];
        let buffer = await readFile(fullPath);
        let compressType = '';
        if (buffer && compress && (0, compressible_1.default)(fullPath)) {
            buffer = await zlibPromise_1.default.zip(buffer);
            compressType = 'gzip';
        }
        let md5 = require('crypto').createHash('md5').update(buffer).digest('hex');
        let size = buffer.length;
        return {
            fullPath,
            relativePath,
            fileName,
            buffer,
            compressType,
            md5,
            size
        };
    }
    exports.default = calcFileInfo;
});
