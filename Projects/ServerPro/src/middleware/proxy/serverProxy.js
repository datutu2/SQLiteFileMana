var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "../../routes/AppFileRoutes", "../../routes/CacheRoutes", "../../routes/DBRoutes"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const AppFileRoutes_1 = __importDefault(require("../../routes/AppFileRoutes"));
    const CacheRoutes_1 = __importDefault(require("../../routes/CacheRoutes"));
    const DBRoutes_1 = __importDefault(require("../../routes/DBRoutes"));
    function serverProxy(app) {
        app.use(AppFileRoutes_1.default.routes());
        app.use(CacheRoutes_1.default.routes());
        app.use(DBRoutes_1.default.routes());
    }
    exports.default = serverProxy;
});
