import appFileRoutes from '../../routes/AppFileRoutes';
import cacheRoutes from '../../routes/CacheRoutes';
import DBRoutes from '../../routes/DBRoutes';

function serverProxy(app: any) {
    app.use(appFileRoutes.routes());
    app.use(cacheRoutes.routes());
    app.use(DBRoutes.routes());
}


export default serverProxy;
