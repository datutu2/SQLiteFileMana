import koaStatic from 'koa-static';
import koaMount from 'koa-mount';
import path from 'path';

import configTool from '../../com/configTool';

function resourcesProxy(app: any, wwwPath: string) {
    app.use(koaStatic(wwwPath), {defer: true});
    // 支持读取 app.zerossl.com 颁发的 https 证书
    app.use(koaMount('/.well-known/pki-validation', koaStatic(path.join(configTool.wwwPath, '/.well-known/pki-validation'))));
    app.use(koaMount('/cacheServer', koaStatic(path.join(configTool.appBasePath, '/temp/fileOut'))));
}


export default resourcesProxy;
