var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "koa-static", "koa-mount", "path", "../../com/configTool"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const koa_static_1 = __importDefault(require("koa-static"));
    const koa_mount_1 = __importDefault(require("koa-mount"));
    const path_1 = __importDefault(require("path"));
    const configTool_1 = __importDefault(require("../../com/configTool"));
    function resourcesProxy(app, wwwPath) {
        app.use((0, koa_static_1.default)(wwwPath), { defer: true });
        app.use((0, koa_mount_1.default)('/.well-known/pki-validation', (0, koa_static_1.default)(path_1.default.join(configTool_1.default.wwwPath, '/.well-known/pki-validation'))));
        app.use((0, koa_mount_1.default)('/cacheServer', (0, koa_static_1.default)(path_1.default.join(configTool_1.default.appBasePath, '/temp/fileOut'))));
    }
    exports.default = resourcesProxy;
});
