import cors from 'koa2-cors';
import compress from 'koa-compress';
import bodyParser from 'koa-bodyparser';
import errHandler from './lib/errHandler';
import logHandler from './lib/logHandler';
import ManaRoutes from '../routes/ManaRoutes';
import IPAuthorityHandler from './lib/IPAuthorityHandler';
import TokenAuthorityHandler from './lib/TokenAuthorityHandler';
import configTool from '../com/configTool';
import AuthorizeRoutes from '../routes/AuthorizeRoutes';

enum RouteType {
    client = 'client',
    manage = 'manage'
}

function manaMiddleWare(app: any) {

    app.use(errHandler());
    app.use(logHandler(RouteType.manage));
    app.use(IPAuthorityHandler(configTool.config.manageWhiteList));
    app.use(TokenAuthorityHandler());

    app.use(compress({br: false}));
    app.use(cors());
    app.use(bodyParser());
    app.use(ManaRoutes.routes());
    app.use(AuthorizeRoutes.routes());
}


export default manaMiddleWare;
