var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "koa2-cors", "koa-compress", "koa-bodyparser", "./lib/errHandler", "./lib/logHandler", "../routes/ManaRoutes", "./lib/IPAuthorityHandler", "./lib/TokenAuthorityHandler", "../com/configTool", "../routes/AuthorizeRoutes"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const koa2_cors_1 = __importDefault(require("koa2-cors"));
    const koa_compress_1 = __importDefault(require("koa-compress"));
    const koa_bodyparser_1 = __importDefault(require("koa-bodyparser"));
    const errHandler_1 = __importDefault(require("./lib/errHandler"));
    const logHandler_1 = __importDefault(require("./lib/logHandler"));
    const ManaRoutes_1 = __importDefault(require("../routes/ManaRoutes"));
    const IPAuthorityHandler_1 = __importDefault(require("./lib/IPAuthorityHandler"));
    const TokenAuthorityHandler_1 = __importDefault(require("./lib/TokenAuthorityHandler"));
    const configTool_1 = __importDefault(require("../com/configTool"));
    const AuthorizeRoutes_1 = __importDefault(require("../routes/AuthorizeRoutes"));
    var RouteType;
    (function (RouteType) {
        RouteType["client"] = "client";
        RouteType["manage"] = "manage";
    })(RouteType || (RouteType = {}));
    function manaMiddleWare(app) {
        app.use((0, errHandler_1.default)());
        app.use((0, logHandler_1.default)(RouteType.manage));
        app.use((0, IPAuthorityHandler_1.default)(configTool_1.default.config.manageWhiteList));
        app.use((0, TokenAuthorityHandler_1.default)());
        app.use((0, koa_compress_1.default)({ br: false }));
        app.use((0, koa2_cors_1.default)());
        app.use((0, koa_bodyparser_1.default)());
        app.use(ManaRoutes_1.default.routes());
        app.use(AuthorizeRoutes_1.default.routes());
    }
    exports.default = manaMiddleWare;
});
