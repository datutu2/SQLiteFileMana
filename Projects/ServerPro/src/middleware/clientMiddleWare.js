var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "koa2-cors", "koa-compress", "koa-bodyparser", "./proxy/resourcesProxy", "./lib/errHandler", "../routes/AppFileRoutes", "../routes/CacheRoutes", "../routes/DBRoutes", "../routes/AuthorizeRoutes", "../com/configTool", "./lib/logHandler", "./lib/IPAuthorityHandler"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const koa2_cors_1 = __importDefault(require("koa2-cors"));
    const koa_compress_1 = __importDefault(require("koa-compress"));
    const koa_bodyparser_1 = __importDefault(require("koa-bodyparser"));
    const resourcesProxy_1 = __importDefault(require("./proxy/resourcesProxy"));
    const errHandler_1 = __importDefault(require("./lib/errHandler"));
    const AppFileRoutes_1 = __importDefault(require("../routes/AppFileRoutes"));
    const CacheRoutes_1 = __importDefault(require("../routes/CacheRoutes"));
    const DBRoutes_1 = __importDefault(require("../routes/DBRoutes"));
    const AuthorizeRoutes_1 = __importDefault(require("../routes/AuthorizeRoutes"));
    const configTool_1 = __importDefault(require("../com/configTool"));
    const logHandler_1 = __importDefault(require("./lib/logHandler"));
    const IPAuthorityHandler_1 = __importDefault(require("./lib/IPAuthorityHandler"));
    var RouteType;
    (function (RouteType) {
        RouteType["client"] = "client";
        RouteType["manage"] = "manage";
    })(RouteType || (RouteType = {}));
    function clientMiddleWare(app) {
        app.use((0, errHandler_1.default)());
        app.use((0, logHandler_1.default)(RouteType.client));
        app.use((0, IPAuthorityHandler_1.default)(['*'], configTool_1.default.config.clientBlackList));
        configTool_1.default.config.clientCORS && app.use((0, koa2_cors_1.default)());
        app.use((0, koa_compress_1.default)({ br: false }));
        (0, resourcesProxy_1.default)(app, configTool_1.default.wwwPath);
        app.use((0, koa_bodyparser_1.default)());
        app.use(CacheRoutes_1.default.routes());
        app.use(AppFileRoutes_1.default.routes());
        app.use(DBRoutes_1.default.routes());
        app.use(AuthorizeRoutes_1.default.routes());
    }
    exports.default = clientMiddleWare;
});
