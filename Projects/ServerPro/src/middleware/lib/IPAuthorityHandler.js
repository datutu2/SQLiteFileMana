(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function IPAuthorityHandler(whiteList = ['*'], blackList = []) {
        return async (ctx, next) => {
            const ip = ctx.request.ip;
            const whiteUserIp = whiteList.find(item => item === ip || item === '*');
            const blackUserIP = blackList.find(item => item === ip);
            if (blackUserIP) {
                ctx.response.status = 401;
                ctx.set('Content-Type', 'application/json;charset=utf-8');
                ctx.response.body = {
                    code: '00000', message: '您已经被加入黑名单，禁止访问！！请联系管理员！！', desc: '非法用户'
                };
            }
            else if (whiteUserIp) {
                await next();
            }
            else {
                ctx.response.status = 401;
                ctx.set('Content-Type', 'application/json;charset=utf-8');
                ctx.response.body = {
                    code: '00000', message: '未授权用户，请联系管理员！！', desc: '当前IP：' + ip
                };
            }
        };
    }
    exports.default = IPAuthorityHandler;
});
