function IPAuthorityHandler(whiteList: string[] = ['*'], blackList: string[] = []) {
    return async (ctx: any, next: Function) => {
        const ip = ctx.request.ip;

        const whiteUserIp = whiteList.find(item => item === ip || item === '*');
        const blackUserIP = blackList.find(item => item === ip);


        if (blackUserIP) {
            // 禁止黑名单内的用户访问
            ctx.response.status = 401;
            ctx.set('Content-Type', 'application/json;charset=utf-8');
            ctx.response.body = {
                code: '00000', message: '您已经被加入黑名单，禁止访问！！请联系管理员！！', desc: '非法用户'
            };
        } else if (whiteUserIp) {
            await next();
        } else {
            ctx.response.status = 401;
            ctx.set('Content-Type', 'application/json;charset=utf-8');
            ctx.response.body = {
                code: '00000', message: '未授权用户，请联系管理员！！', desc: '当前IP：' + ip
            };
        }

    };
}


export default IPAuthorityHandler;
