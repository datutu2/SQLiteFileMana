var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "fs", "../../com/configTool", "path"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const fs_1 = __importDefault(require("fs"));
    const configTool_1 = __importDefault(require("../../com/configTool"));
    const path_1 = __importDefault(require("path"));
    var RouteType;
    (function (RouteType) {
        RouteType["client"] = "client";
        RouteType["manage"] = "manage";
    })(RouteType || (RouteType = {}));
    function logHandler(routeType) {
        return async (ctx, next) => {
            await configTool_1.default.init();
            const logObj = {
                ip: ctx.request.ip,
                method: ctx.request.method,
                start: new Date().toLocaleString(),
                passSecond: 0,
                url: decodeURI(ctx.request.url)
            };
            await next();
            if (logObj.url.includes('/Resources/') || logObj.url.includes('/favicon.ico')) {
                return;
            }
            logObj.passSecond = (new Date().getTime() - new Date(logObj.start).getTime()) / 1000;
            if (routeType === RouteType.client) {
                fs_1.default.appendFileSync(path_1.default.join(configTool_1.default.appBasePath, '/temp/logs/clientServer/WebLog.json'), JSON.stringify(logObj) + ',\n');
            }
            else {
                fs_1.default.appendFileSync(path_1.default.join(configTool_1.default.appBasePath, '/temp/logs/manageServerPort/WebLog.json'), JSON.stringify(logObj) + ',\n');
            }
        };
    }
    exports.default = logHandler;
});
