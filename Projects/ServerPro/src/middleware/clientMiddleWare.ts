import cors from 'koa2-cors';
import compress from 'koa-compress';
import bodyParser from 'koa-bodyparser';
import resourcesProxy from './proxy/resourcesProxy';
import errHandler from './lib/errHandler';
import appFileRoutes from '../routes/AppFileRoutes';
import cacheRoutes from '../routes/CacheRoutes';
import DBRoutes from '../routes/DBRoutes';
import AuthorizeRoutes from '../routes/AuthorizeRoutes';
import configTool from '../com/configTool';
import logHandler from './lib/logHandler';
import IPAuthorityHandler from './lib/IPAuthorityHandler';

enum RouteType {
    client = 'client',
    manage = 'manage'
}

function clientMiddleWare(app: any) {
    app.use(errHandler());
    app.use(logHandler(RouteType.client));
    app.use(IPAuthorityHandler(['*'], configTool.config.clientBlackList));

    // 允许跨域请求
    configTool.config.clientCORS && app.use(cors());

    app.use(compress({br: false}));

    resourcesProxy(app,configTool.wwwPath);

    // 最后再进行 post 请求格式化
    app.use(bodyParser());

    // 数据转发缓存服务
    app.use(cacheRoutes.routes());


    app.use(appFileRoutes.routes());
    app.use(DBRoutes.routes());
    app.use(AuthorizeRoutes.routes());
}

export default clientMiddleWare;
