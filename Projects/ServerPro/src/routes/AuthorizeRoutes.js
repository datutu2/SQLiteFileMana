var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "koa-router", "../lib/RSATool", "../com/configTool", "../lib/getServerInfo"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const koa_router_1 = __importDefault(require("koa-router"));
    const RSATool_1 = __importDefault(require("../lib/RSATool"));
    const configTool_1 = __importDefault(require("../com/configTool"));
    const getServerInfo_1 = __importDefault(require("../lib/getServerInfo"));
    const AuthorizeRoutes = new koa_router_1.default({ prefix: '/AuthorizeServer' });
    AuthorizeRoutes
        .get('/login', async (ctx) => {
        let { userName, password } = ctx.request.query;
        const userInfo = configTool_1.default.config.userList.find((item) => item.userName === userName && item.password === password);
        ctx.set('Content-Type', 'application/json;charset=utf-8');
        if (userInfo) {
            const serverInfo = (0, getServerInfo_1.default)();
            const userCopy = { ...userInfo };
            delete userCopy.password;
            userCopy.ip = ctx.request.ip;
            userCopy.token_expired_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 7).toISOString();
            userCopy.token = RSATool_1.default.encrypt(JSON.stringify(userCopy), configTool_1.default.config.publicDer);
            ctx.body = JSON.stringify({
                code: 200,
                message: '登录成功',
                data: { serverInfo, userInfo: userCopy }
            });
            ctx.status = 200;
        }
        else {
            ctx.body = JSON.stringify({
                code: 401,
                message: '登录失败，用户名或密码错误',
                data: null
            });
            ctx.status = 401;
        }
    });
    exports.default = AuthorizeRoutes;
});
