var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "fs", "axios", "path", "fs-promise", "koa-router", "../lib/ContentTypeTool", "../lib/awaitWrap", "../com/configTool", "../lib/FSTool/index"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const fs_1 = __importDefault(require("fs"));
    const axios_1 = __importDefault(require("axios"));
    const path_1 = __importDefault(require("path"));
    const fs_promise_1 = __importDefault(require("fs-promise"));
    const koa_router_1 = __importDefault(require("koa-router"));
    const ContentTypeTool_1 = __importDefault(require("../lib/ContentTypeTool"));
    const awaitWrap_1 = __importDefault(require("../lib/awaitWrap"));
    const configTool_1 = __importDefault(require("../com/configTool"));
    const index_1 = __importDefault(require("../lib/FSTool/index"));
    const cacheRoutes = new koa_router_1.default({ prefix: '/cacheServer' });
    cacheRoutes
        .get('/:protocol/:ip/:port/:url(.*)', async (ctx) => {
        let protocol = ctx.params.protocol === 'http' ? 'http://' : 'https://';
        let ip = ctx.params.ip;
        let port = ctx.params.port;
        let url = ctx.params.url;
        let reqUrl = `${protocol}${ip}:${port}/${url}`;
        let fileSavePath = path_1.default.join(configTool_1.default.appBasePath, 'temp/fileOut', `${ctx.params.protocol}/${ip}/${port}/${url}`);
        let fileDir = path_1.default.dirname(fileSavePath);
        if (url) {
            let [err, res] = await index_1.default.readFileAsync(fileSavePath);
            if (res) {
                let contentType = ContentTypeTool_1.default.getContentType(fileSavePath);
                ctx.set('Content-Type', contentType);
                ctx.body = res;
            }
            if (!res) {
                [err, res] = await (0, awaitWrap_1.default)(axios_1.default.get(reqUrl, { responseType: 'arraybuffer' }));
                if (err) {
                    ctx.status = 422;
                    ctx.body = err;
                }
                else {
                    for (const resKey in res.headers) {
                        ctx.set(resKey, res.headers[resKey]);
                    }
                    ctx.body = res.data;
                    if (configTool_1.default.preservableONFile()) {
                        if (!path_1.default.extname(fileSavePath)) {
                            fileSavePath += ContentTypeTool_1.default.getExt(res.headers['content-type']);
                        }
                        await fs_promise_1.default.ensureDir(fileDir);
                        fs_1.default.writeFileSync(fileSavePath, res.data);
                    }
                }
            }
        }
    });
    exports.default = cacheRoutes;
});
