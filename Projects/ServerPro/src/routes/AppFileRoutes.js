var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "koa-router", "../com/DBTool/DBConnectTool", "../lib/ContentTypeTool"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const koa_router_1 = __importDefault(require("koa-router"));
    const DBConnectTool_1 = __importDefault(require("../com/DBTool/DBConnectTool"));
    const ContentTypeTool_1 = __importDefault(require("../lib/ContentTypeTool"));
    const appFileRoutes = new koa_router_1.default({ prefix: '/appFile' });
    appFileRoutes
        .get('/getFileByFullPath', async (ctx) => {
        let { DBName, fullPath } = ctx.request.query;
        if (DBName && fullPath) {
            let dbTool = await DBConnectTool_1.default.openDB(DBName);
            let file = await dbTool.getFileByFullPath(fullPath);
            if (!file || !file.file_data) {
                throw new Error('文件不存在，路径：' + fullPath);
            }
            else {
                let contentType = ContentTypeTool_1.default.getContentType(file.file_name);
                ctx.set('Content-Type', contentType);
                ctx.set('content-Disposition', `attachment;filename=${file.file_name}`);
                ctx.body = file.file_data;
            }
        }
        else {
            ctx.status = 404;
        }
    })
        .get('/getFileByMd5', async (ctx) => {
        let { DBName, md5 } = ctx.request.query;
        if (DBName && md5) {
            let dbTool = await DBConnectTool_1.default.openDB(DBName);
            let file = await dbTool.getFileByMd5(md5);
            if (!file || !file.file_data) {
                throw new Error('文件不存在，MD5：' + md5);
            }
            else {
                let contentType = ContentTypeTool_1.default.getContentType(file.file_name);
                ctx.set('Content-Type', contentType);
                ctx.set('content-Disposition', `attachment;filename=${file.file_name}`);
                ctx.body = file.file_data;
            }
        }
        else {
            ctx.status = 404;
        }
    });
    exports.default = appFileRoutes;
});
