var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "koa", "child_process", "colors-console", "./com/configTool", "./middleware/clientMiddleWare", "./middleware/manaMiddleWare", "./com/webSocketTool/webSocketTool", "./lib/portIsOccupied"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const koa_1 = __importDefault(require("koa"));
    const child_process_1 = require("child_process");
    const colors_console_1 = __importDefault(require("colors-console"));
    const configTool_1 = __importDefault(require("./com/configTool"));
    const clientMiddleWare_1 = __importDefault(require("./middleware/clientMiddleWare"));
    const manaMiddleWare_1 = __importDefault(require("./middleware/manaMiddleWare"));
    const webSocketTool_1 = __importDefault(require("./com/webSocketTool/webSocketTool"));
    const portIsOccupied_1 = __importDefault(require("./lib/portIsOccupied"));
    const WS = require('ws');
    const clientAPP = new koa_1.default();
    const manageAPP = new koa_1.default();
    async function main() {
        (0, child_process_1.exec)('reg add HKEY_CURRENT_USER\\Console /v QuickEdit /t REG_DWORD /d 00000000 /f');
        await configTool_1.default.init();
        const clientServerPort = configTool_1.default.config.clientServerPort;
        const manageServerPort = configTool_1.default.config.manageServerPort;
        const wsServerPort = configTool_1.default.config.wsServerPort;
        if (clientServerPort !== await (0, portIsOccupied_1.default)(clientServerPort, false)) {
            console.log((0, colors_console_1.default)('red', `================== 端口号 ${clientServerPort} 被占用，程序启动失败 ==================`));
            return;
        }
        if (manageServerPort !== await (0, portIsOccupied_1.default)(manageServerPort, false)) {
            console.log((0, colors_console_1.default)('red', `================== 端口号 ${manageServerPort} 被占用，程序启动失败 ==================`));
            return;
        }
        await new Promise((resolve, reject) => {
            clientAPP.listen(clientServerPort, function () {
                console.log(`【启动 HTTP  Web服务器】：URL:【http://127.0.0.1:${clientServerPort}】，静态网站资源目录：【${configTool_1.default.wwwPath}】`);
                (0, clientMiddleWare_1.default)(clientAPP);
                resolve(null);
            });
        });
        await new Promise((resolve, reject) => {
            if (manageServerPort) {
                const manageServer = manageAPP.listen(manageServerPort, function () {
                    const manageMessage = `【启动 管理员接口服务】：URL:【http://127.0.0.1:${manageServerPort}】`;
                    if (wsServerPort === manageServerPort) {
                        const wss = new WS.Server({ server: manageServer });
                        webSocketTool_1.default.start(wss);
                        console.log(manageMessage + `与【WebSocket】共用【${manageServerPort}】端口`);
                    }
                    else {
                        const wsAPP = new koa_1.default();
                        const wsServer = wsAPP.listen(wsServerPort, function () {
                            const wss = new WS.Server({ server: wsServer });
                            webSocketTool_1.default.start(wss);
                            console.log(manageMessage + `；【启动 WebSocket 服务】：占用端口:【${wsServerPort}】`);
                        });
                    }
                    (0, manaMiddleWare_1.default)(manageAPP);
                    resolve(null);
                });
            }
        });
        console.log((0, colors_console_1.default)('green', `================== 服务器启动成功！当前版本：${configTool_1.default.config.version} ==================`));
    }
    main().then();
});
