const dialog = require('electron').dialog;
const exec = require('child_process').exec;
const {fdir} = require('fdir');

let FileSystemTool = {};

// 文件夹选取框
FileSystemTool.choosePathDialog = async function () {
    let paths = await dialog.showOpenDialog({properties: ['openDirectory']});
    let path = paths.filePaths.pop();
    return path;
};

// 文件选择器，加载 .sqlite3 文件
FileSystemTool.chooseFileDBDialog = async function () {
    let paths = await dialog.showOpenDialog({
        title: '打开数据库文件',
        filters: [{
            name: 'sqlite3',
            extensions: ['sqlite3']
        }]
    });
    let filePath = paths.filePaths.pop();
    return filePath;
};

// 打开文件夹
FileSystemTool.openCatalogue = async function (path) {
    exec(`explorer.exe /select,${path}`);
};

// 扫描文件夹
FileSystemTool.getFilePathList = async function (parentPath) {
    const api = new fdir().withFullPaths().crawl(parentPath);
    return await api.withPromise();
};


module.exports = FileSystemTool;
